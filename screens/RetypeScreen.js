import React from "react";
import { Form } from "native-base";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Alert,
  ImageBackground,
  KeyboardAvoidingView,
  ScrollView
} from "react-native";
import { Header } from "react-navigation";

import InputText from "../components/InputText";
import background from '../assets/images/forget.png';
import { apiUrl } from "../config/api";
import axios from "axios";

class SignUpScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      RetypePassword: "",
      user_id: this.props.navigation.getParam("user_id")
    };
  }

  onSubmit = () => {
    let { password, RetypePassword, user_id } = this.state;
    if (password.length < 6) {
      Alert.alert("Alert", "Password must be 6-15 characters long.");
    } else if (password.length > 15) {
      Alert.alert("Alert", "Password cannot be longer than 15 characters.");
    } else if (RetypePassword.length < 6) {
      Alert.alert("Alert", "Re - Type Password must be 6-15 characters long.");
    } else if (RetypePassword.length > 15) {
      Alert.alert(
        "Alert",
        "Re - Type Password cannot be longer than 15 characters."
      );
    } else {
      axios
        .get(apiUrl + "/change_password", {
          params: {
            password: password,
            confirm_password: RetypePassword,
            user_id: user_id
          }
        })
        .then(function(response) {
          Alert.alert("Alert", response.data.message);
        })
        .catch(function(error) {
          Alert.alert("Alert", "Bad request error.");
        });
    }
  };

  render() {
    const { seg } = this.state;
    var { height, width } = Dimensions.get("window");

    return (
      <ImageBackground style={{ flex: 1 }} source={background}>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          keyboardVerticalOffset={Header.HEIGHT + 90}
          behavior="padding"
        >
          <ScrollView>
            <View style={{ height: height * 0.3 }} />
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  color: "grey",
                  fontSize: 20,
                  marginLeft: 30,
                  fontWeight: "bold"
                }}
              >
                Enter New Password
              </Text>
              <Form style={{ marginTop: 30 }}>
                <InputText
                  icon="lock-outline"
                  placeholder="Password"
                  autoCapitalize="none"
                  value={this.state.password}
                  secureTextEntry={true}
                  onChangeText={password => this.setState({ password })}
                />
                <InputText
                  icon="lock-outline"
                  placeholder="Re -Type Password"
                  autoCapitalize="none"
                  value={this.state.RetypePassword}
                  secureTextEntry={true}
                  onChangeText={RetypePassword =>
                    this.setState({ RetypePassword })
                  }
                />
              </Form>

              <View style={{ marginTop: 60 }}>
                <TouchableOpacity onPress={this.onSubmit}>
                  <View
                    style={[
                      {
                        backgroundColor: "#df1d25",
                        width: width * 0.7,
                        alignItems: "center",
                        width: 300,
                        paddingVertical: 8,
                        marginBottom: 5,
                        alignSelf: "center"
                      }
                    ]}
                  >
                    <Text
                      style={[
                        {
                          fontSize: 18,
                          textAlign: "center",
                          color: "white",
                          fontWeight: "bold"
                        }
                      ]}
                    >
                      Confirm
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

export default SignUpScreen;
