import React from "react";
import { commonStyles } from "./styles";
import { Segment, Button, Form, CheckBox } from "native-base";
import { StackActions, NavigationActions } from 'react-navigation';

import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Alert,
  StatusBar,
  Image,
  ImageBackground,
  KeyboardAvoidingView,BackHandler,
  ScrollView,AsyncStorage,ActivityIndicator
} from "react-native";
import InputText from "../components/InputText";
import logo from "../assets/images/logo.png";
import background from "../assets/images/sign-in-background.jpg";
import { Constants, Expo } from "expo";
import * as Facebook from 'expo-facebook';
import * as Google from 'expo-google-app-auth';

import google from "../assets/images/search.png";
import facebook from "../assets/images/FB-icon.png";
import { apiUrl } from "../config/api";
import axios from "axios";
import { WP } from "../responsive";

class SignInScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: "",
      password: "",
      checked: false,
      myKey:"",
      showLumper:false
    };
  }
//   componentWillMount() {
//     BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
// }

// componentWillUnmount() {
//     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
// }

// handleBackButton = () => {
//     //add your code
//     BackHandler.exitApp()

//     return true;
// };

  signIn = async () => {
    try {
      const result = await Google.logInAsync({
        androidClientId:
          "47491630978-ucq7ad6qjsp58dfuk57b6c5hbm917tla.apps.googleusercontent.com",
        iosClientId:
          "47491630978-7k0svtqfqv6gvebtrk8muhv9lpoan5pi.apps.googleusercontent.com",
        scopes: ["profile", "email"]
      });

      if (result.type === "success") {
        console.log("success",result)
        if(result){
        //   var token = {
        //     userid:result.user.id,
        //     name:result.user.name,
        //     phoneno: null,
        //     lastname:null,
        //     useremail:result.user.email,
        //     profilepic:result.user.photoUrl
        //   }
        //   this.saveKey(JSON.stringify(token))
      



        // this.props.navigation.navigate("WelcomeScreen2");
        axios
        .get(apiUrl + "/social_media_login", {
          params: {
            id:result.user.id,
          name:result.user.name,
          email:result.user.email,
            
          }
        })
        // .then((response) =>{
        //   if (response.data.result == "false") {
        //     Alert.alert("Alert", response.data.message);
        //   } else {
        
        //   }
        // })
        .then(
          (response) => {
            console.log(response);
                 if (response.data.result == "false") {
            Alert.alert("Alert", response.data.message);
          }  else {
            console.log("showing response",response)
            var token = {
              userid:response.data.id,
              name:response.data.name,
              phoneno: null,
              lastname:null,
              useremail:response.data.email,
              profilepic:result.user.photoUrl
            }
            this.saveKey(JSON.stringify(token))

            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'WelcomeScreen2' })],
            });
            this.props.navigation.dispatch(resetAction);
          }
          }
        )
        .catch((error)=> {
          this.setState({showLumper:false})

          console.log("error",error)
          Alert.alert("Alert", "Bad request error.");
        });
        

      }
      } else {
        console.log("cancelled");
      }
    } catch (e) {
      console.log("error", e);
    }
  };

  FacebookLogin = async () => {
    try {
      //47491630978-ucq7ad6qjsp58dfuk57b6c5hbm917tla.apps.googleusercontent.com
      const data = await Facebook.logInWithReadPermissionsAsync(
        "394038394827648",
        { permissions: ["public_profile", "email"] }
      );
      console.log("data", data);
      switch (data.type) {
        case "success": {
          // Get the user's name using Facebook's Graph API
          const response = await fetch(
            `https://graph.facebook.com/me?fields=id,name,email&access_token=${data.token}`
          );
          const profile = await response.json();
          console.log("response", response);
          console.log("profile", profile);
          Alert.alert("Logged in!", `Hi ${profile.name}!`);
          if(profile){
          
            axios
            .get(apiUrl + "/social_media_login", {
              params: {
                id:profile.id,
              name:profile.name,
              email:profile.email,
                
              }
            })
            // .then((response) =>{
            //   if (response.data.result == "false") {
            //     Alert.alert("Alert", response.data.message);
            //   } else {
            
            //   }
            // })
            .then(
              (response) => {
                console.log(response);
                     if (response.data.result == "false") {
                Alert.alert("Alert", response.data.message);
              }  else {
                var token = {
                  userid:response.data.id,
                  name:response.data.name,
                  phoneno: null,
                  lastname:"",
                  useremail:response.data.email,
                  profilepic:`https://graph.facebook.com/${profile.id}/picture`
                }
                this.saveKey(JSON.stringify(token))
    
                const resetAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: 'WelcomeScreen2' })],
                });
                this.props.navigation.dispatch(resetAction);              }
              }
            )
            .catch((error)=> {
              this.setState({showLumper:false})
    
              console.log("error",error)
              Alert.alert("Alert", "Bad request error.");
            });

          }
       
          break;
        }
        case "cancel": {
          Alert.alert("Cancelled!", "Login was cancelled!");
          break;
        }
        default: {
          Alert.alert("Oops!", "Login failed!");
        }
      }
    } catch (e) {
      Alert.alert("Oops!", "Login failed!");
      console.log("login failed error",e)
    }
  };

  onclickData = () => {
    let { checked } = this.state;
    this.setState({
      checked: !checked
    });
  };
  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@MySuperStore:key');
      this.setState({myKey: value});
    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  async saveKey(value) {
    try {
      await AsyncStorage.setItem('@MySuperStore:key', value);
    } catch (error) {
      console.log("Error saving data" + error);
    }
  }

  onSubmit = () => {

    let { phone, password,checked } = this.state;
    const { navigate } = this.props.navigation;
    var phoneno = /^\d+$/;
    if (!phoneno.test(this.state.phone)) {
      Alert.alert("Alert", "Mobile No is empty or invalid.");
    } else if (this.state.password.length < 6) {
      Alert.alert("Alert", "Password must be 6-15 characters long.");
    } else if (this.state.password.length > 15) {
      Alert.alert("Alert", "Password cannot be longer than 15 characters.");
    } else {
      this.setState({showLumper:true})

      axios
        .get(apiUrl + "/getlogin", {
          params: {
            keyword: phone,
            password: password
          }
        })
        // .then((response) =>{
        //   if (response.data.result == "false") {
        //     Alert.alert("Alert", response.data.message);
        //   } else {
        
        //   }
        // })
        .then(
          (response) => {
            this.setState({showLumper:false})
            console.log(response);
                 if (response.data.result == "false") {
            Alert.alert("Alert", response.data.message);
          }  else {
            // if(this.state.checked){
            //   var token = {
            //     name:response.data.firstname,
            //     phoneno: phone,
            //     lastname:response.data.lastname,
            //     useremail:response.data.user_email
            //   }
            //   this.saveKey(JSON.stringify(token))


            // }
            var token = {
              userid:response.data.user_id,
              name:response.data.firstname,
              phoneno: phone,
              lastname:response.data.lastname,
              useremail:response.data.user_email,
              profilepic:null
            }
            this.saveKey(JSON.stringify(token))

const resetAction = StackActions.reset({
  index: 0,
  actions: [NavigationActions.navigate({ routeName: 'WelcomeScreen2' })],
});
this.props.navigation.dispatch(resetAction);

            // navigate("WelcomeScreen2");
          }
          }
        )
        .catch((error)=> {
          this.setState({showLumper:false})

          console.log("error",error)
          Alert.alert("Alert", "Bad request error.");
        });
    }
  };

  render() {
    const { seg } = this.state;
    var { height, width } = Dimensions.get("window");

    return (
      <ScrollView contentContainerStyle = {{flexGrow:1}}>

      <ImageBackground style={{ flex: 1 }} source={background}>
          <View style = {{display:'flex',height:WP('55'),width:"100%",alignItems:'center',justifyContent:"flex-end"}}>
          <Image
        source={require("../assets/images/logo.png")}
        style={{ width: WP('50'), height: WP('50'), resizeMode: "contain",marginTop:WP('3') }}
      />

          </View>
          <View style={{display:'flex'}} />
          <Form style={{ }}>
            <InputText
              icon="phone-iphone"
              placeholder="03xxxxxxxxx"
              onChangeText={text => this.setState({ phone: text })}
              value={this.state.phone}
              keyboardType={'phone-pad'}

            />

            <InputText
              icon="lock-outline"
              placeholder="Password"
              autoCapitalize="none"
              value={this.state.password}
              secureTextEntry={true}
              onChangeText={password => this.setState({ password })}
            />
            
            

            <View
              style={{
                alignSelf: "flex-end",
                marginTop: 8,
                marginRight: 20
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <CheckBox
                  onPress={this.onclickData}
                  checked={this.state.checked}
                  color="grey"
                  style={{ marginRight: 20 }}
                />
                <Text style={{ color: "grey" }}>Remember Password</Text>
              </View>
            </View>
          </Form>
          <View style={{ marginTop: 20 }}>
          {this.state.showLumper?
              <View onPress={this.onSubmit}>
              <View
                style={[
                  {
                    backgroundColor: "#df1d25",
                    width: width * 0.7,
                    alignItems: "center",
                    width: 300,
                    paddingVertical: 8,
                    marginBottom: 5,
                    alignSelf: "center"
                  }
                ]}
              >
                       <ActivityIndicator size="small" color="#fff" />

              </View>
            </View>
          :
          <TouchableOpacity onPress={this.onSubmit}>
          <View
            style={[
              {
                backgroundColor: "#df1d25",
                width: width * 0.7,
                alignItems: "center",
                width: 300,
                paddingVertical: 8,
                marginBottom: 5,
                alignSelf: "center"
              }
            ]}
          >
            <Text
              style={[
                {
                  fontSize: 18,
                  textAlign: "center",
                  color: "white",
                  fontWeight: "bold"
                }
              ]}
            >
              Sign In
            </Text>
          </View>
        </TouchableOpacity>

          }
        
          </View>

          <View
            style={{ flexDirection: "row", alignSelf: "center", marginTop: 10 }}
          >
            <View
              style={{
                borderColor: "grey",
                borderWidth: 1,
                width: 100,
                height: 0,
                top: 10
              }}
            />
            <Text style={{ color: "grey" }}> or Sign up </Text>
            <View
              style={{
                borderColor: "grey",
                borderWidth: 1,
                width: 100,
                height: 0,
                top: 10
              }}
            />
          </View>

          {/* <View
            style={{
              marginTop: 20,
              flexDirection: "row",
              alignSelf: "center"
            }}
          >
            <TouchableOpacity
              onPress={this.FacebookLogin}
              style={{ marginRight: 10 }}
            >
              <View
                style={[
                  {
                    backgroundColor: "#3a5898",
                    width: width * 0.7,
                    alignItems: "center",
                    width: 140,
                    paddingVertical: 8,
                    marginBottom: 5,
                    alignSelf: "center"
                  }
                ]}
              >
                <Image
                  resizeMode={"contain"}
                  style={[
                    {
                      height: height * 0.04,
                      width: width * 0.065
                    }
                  ]}
                  source={facebook}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.signIn} style={{ marginLeft: 10 }}>
              <View
                style={[
                  {
                    backgroundColor: "#e2ebf0",
                    width: width * 0.7,
                    alignItems: "center",
                    width: 140,
                    paddingVertical: 8,
                    marginBottom: 5,
                    alignSelf: "center"
                  }
                ]}
              >
                <Image
                  resizeMode={"contain"}
                  style={[
                    {
                      height: height * 0.035,
                      width: width * 0.05
                    }
                  ]}
                  source={google}
                />
              </View>
            </TouchableOpacity>
          </View> */}
          <View style={{ marginTop: 20 }}>
          
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("SignUpScreen");
              }}
            >
              <View
                style={[
                  {
                    backgroundColor: "#df1d25",
                    width: width * 0.7,
                    alignItems: "center",
                    width: 300,
                    paddingVertical: 8,
                    marginBottom: 5,
                    alignSelf: "center"
                  }
                ]}
              >
                <Text
                  style={[
                    {
                      fontSize: 18,
                      textAlign: "center",
                      color: "white",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  Sign Up
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("ForgotScreen");
              }}
            >
              <Text
                style={{ alignSelf: "center", marginTop: 15, color: "grey" }}
              >
                Forgot password
              </Text>
            </TouchableOpacity>
          </View>
      </ImageBackground>
      </ScrollView>

    );
  }
}

export default SignInScreen;
