import React from "react";
import { Form } from "native-base";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Alert,
  ImageBackground,
  KeyboardAvoidingView,
  ScrollView,ActivityIndicator,BackHandler
} from "react-native";
import { Header } from "react-navigation";
import { apiUrl } from "../config/api";
import { StackActions, NavigationActions } from 'react-navigation';

import InputText from "../components/InputText";
import background from '../assets/images/artWork.jpeg';
import axios from "axios";
import { WP } from "../responsive";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class SignUpScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      last: "",
      phone: "",
      email: "",
      password: "",
      showlumper:false,
    };
  }

  onSubmit = () => {
    let p = this.props;
    let { name, last, phone, email, password } = this.state;
    var phoneno = /^((?:00|\+)92)?(0?3(?:[0-46]\d|55)\d{7})$/  //Alternatively ^0\d{3}-\d{7}$;
    var checkit = /^[a-zA-Z ]+$/
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!checkit.test(name) || name.length < 3) {
      Alert.alert(
        "Alert",
        "Name must be 3-25 characters long. Do not enter numbers."
      );
    } else if (last.match(/\d+/g) || last.length < 2) {
      Alert.alert(
        "Alert",
        "Last Name must be 2-25 characters long. Do not enter numbers."
      );
    } else if (!phoneno.test(phone)) {
      Alert.alert("Alert", "Mobile No is empty or invalid.");
    } else if (!this.state.email) {
      Alert.alert("Alert", "Email address is empty or invalid.");
    } else if (password.length < 6) {
      Alert.alert("Alert", "Password must be 6-15 characters long.");
    } else if (password.length > 15) {
      Alert.alert("Alert", "Password cannot be longer than 15 characters.");
    } else {
      this.setState({showLumper:true})

      axios
        .get(apiUrl + "/signup", {
          params: {
            firstname: name,
            lasname: last,
            password: password,
            email: email,
            phone_number: phone
          }
        })
        .then((response)=>{
          console.log("dreams",response)
          this.setState({showLumper:false})
          if (response.data instanceof Array) {
            if (response.data[0].result == "false") {
              Alert.alert("Alert", response.data[0].message);

            }
          } else {

            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'VerificationScreen' })],
            });
            this.props.navigation.dispatch(resetAction);
          }
        })
        .catch((error) =>{
          this.setState({showLumper:false})
          console.log(">", error);
          Alert.alert("Alert", "Error in created account.");
        });

    }
  };

  render() {
    var { height, width } = Dimensions.get("window");
    let { name, last, phone, email, password } = this.state;
    return (
     
   <KeyboardAvoidingView
          style={{ flex: 1 }}
          keyboardVerticalOffset={Header.HEIGHT + 90}
          behavior="padding"
        >
      <ImageBackground style={{ flex: 1 }} source={background}>
      
            <View style={{ height: height * 0.25 }} />
            <View style={{ flex: 1 }}>
              <Form>
                <InputText
                  icon="person-outline"
                  placeholder="First Name(Minimum 3 Character)"
                  onChangeText={text => this.setState({ name: text })}
                  value={name}
                  keyboardType = {'email-address'}
                  
                />
                <InputText
                  icon="person-outline"
                  placeholder="Last Name"
                  onChangeText={text => this.setState({ last: text })}
                  value={last}
                />
                <InputText
                  icon="call"
                  placeholder="03xxxxxxxxx"
                  onChangeText={text => this.setState({ phone: text })}
                  value={phone}
                  keyboardType={'phone-pad'}
                />
                <InputText
                  icon="mail-outline"
                  placeholder="Email"
                  onChangeText={text => this.setState({ email: text })}
                  value={email}
                />
         

                <InputText
                  icon="lock-outline"
                  placeholder="Password"
                  autoCapitalize="none"
                  value={password}
                  secureTextEntry={true}
                  onChangeText={password => this.setState({ password })}
                />
              </Form>

              <View style={{ marginTop: 30 }}>
              {this.state.showlumper?
              <View >
              <View
                style={[
                  {
                    backgroundColor: "#df1d25",
                    width: width * 0.7,
                    alignItems: "center",
                    width: 300,
                    paddingVertical: 8,
                    marginBottom: 5,
                    alignSelf: "center"
                  }
                ]}
              >
                                    <ActivityIndicator size="small" color="#fff" />

              </View>
            </View>
              :
              <TouchableOpacity onPress={this.onSubmit}>
                  <View
                    style={[
                      {
                        backgroundColor: "#df1d25",
                        // width: width * 0.7,
                        // alignItems: "center",
                        // width: 300,
                        // paddingVertical: 8,
                        // marginBottom: 5,
                        // alignSelf: "center"
                        display:"flex",
                        height:WP('10'),
                        width:WP('70'),
                        alignItems:'center',
                        alignSelf:'center',
                        justifyContent:'center',
                        marginTop:WP('5')
                      }
                    ]}
                  >
                    <Text
                      style={[
                        {
                          fontSize: 18,
                          textAlign: "center",
                          color: "white",
                          fontWeight: "bold"
                        }
                      ]}
                    >
                      Sign Up
                    </Text>
                  </View>
                </TouchableOpacity>

              }
                
              </View>
            </View>
      </ImageBackground>
              </KeyboardAvoidingView>

     

    );
  }
}

export default SignUpScreen;
