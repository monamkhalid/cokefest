import React from 'react';
import {  Header, Left, Body, Right,Button,Icon } from 'native-base';
import {
    View,
    Text,
    Dimensions,
ImageBackground,
ScrollView,
StyleSheet,
} from 'react-native';
import background from '../assets/images/artWork.jpeg';
import { WP } from '../responsive';


class HelpScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <ImageBackground style={{ width: '100%', height: '100%', }} source={background}>
                <Header
          hasSegment
          style={[{ backgroundColor: "#E52526", marginTop: 22,height:WP('20')  }]}
        >
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialIcons"
                name="arrow-back"
                style={[{ color: "white" }]}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={[{ color: "white", alignSelf: "flex-start", fontSize: 18 }]}
            >
            Rules and Regulations 
            </Text>
          </Body>
          <Right></Right>
        </Header>
  {/* <View style={{backgroundColor:'red',height:30,justifyContent:"center",}}>
                        <Text style={{marginLeft:20,color:'white'}}>Rules & Regulation</Text>
                        </View> */}
                        {/* <View style = {{height:1-}}>

                        </View> */}
                        <ScrollView style = {{flexGrow:1}}>

                     <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>1. Ticket is a revocable license. It may be revoked by Management and admission can be refused for any reason.</Text>
                     <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>2. Management reserves the right to cancel/Postpone the event at their own discretion.</Text>
                     <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>3. The Soul Festival is a Family Friendly event & is for families only. No Single Males are allowed.</Text>
                     <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>4. Outside food and beverages are not allowed.</Text>
                     <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>5. Tickets obtained from unauthorized sources are void.</Text>
                     <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>6. Management shall not be responsible for any loss, injury or damage you may suffer during the festival.</Text>
                     <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>7. Children under 12 must be supervised by an adult. Children under 2 are admitted for free when accompanied by an adult.</Text>
                     <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>8. The ticket is non-refundable and cannot be redeemed for cash.</Text>
                     <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>9. Upon purchase of a ticket, you give consent to Soul Festival and it's partners to utilize your picture  or video with your presence, as part of their social media campaigns and visibility.</Text>
                     <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>10. Lost or stolen tickets will not be re-issued.</Text>    
          <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>11. Upon entry you will be issued with a wristband – this is your proof that you have been permitted entry. Without the wristband you cannot enter the venue.</Text>  
          <Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>12. All attendees will be searched upon entering the festival grounds. Kindly maintain respect and coordinate. Individuals pushing others or harassing female attendees will not be entertained.
</Text>    
<Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>13. Please use the litter bins and recycling points provided. </Text>    
<Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12}}>14. All kinds of illegal substances are prohibited.</Text>    
<Text style={{marginLeft:15,marginRight:15,marginTop:10,fontSize:12,marginBottom:WP('30')}}>15. Have fun but act responsibly – consider your own safety and that of other guests.</Text>    

</ScrollView>

           
      </ImageBackground>
              
             
    


        );
    }
}

export default HelpScreen;


var styles = StyleSheet.create({
    column: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        width: 200
    },
    row: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        flex: 1
    },
    bullet: {
        width:10,
        fontSize:30,
        fontWeight:"bold"
    },
    bulletText: {
        flex: 1
    },
    boldText: {
        fontWeight: 'bold'
    },
    normalText: {
    }
});