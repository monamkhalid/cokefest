import React from "react";
import { Header, Left, Body, Right, Button, Icon } from "native-base";
import {
  View,
  Text,
  Dimensions,
  ImageBackground,
  FlatList
} from "react-native";
import Swiper1 from "../assets/images/Swiper1.jpg";
import { apiUrl } from "../config/api";
import axios from "axios";
import {WP} from '../responsive'
class Merchandise extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
  }

  componentWillMount() {
    let state = this;
    axios
      .get(apiUrl + "/getmerchandise")
      .then(function(response) {
        state.setState({ items: response.data });
      })
      .catch(function(error) {
        Alert.alert("Alert", "Bad request error.");
      });
  }

  render() {
    var { height, width } = Dimensions.get("window");
    let { items } = this.state;
    return (
      <View style={{ flex: 1, }}>
      
        <Header
          hasSegment
          style={[{ backgroundColor: "#E52526", marginTop: 22,height:WP('20')  }]}
        >
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialIcons"
                name="arrow-back"
                style={[{ color: "white" }]}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={[{ color: "white", alignSelf: "flex-start", fontSize: 18 }]}
            >
            Gallery
            </Text>
          </Body>
          <Right></Right>
        </Header>
       

          <FlatList
            style = {{display:'flex'}}
            data={items}
            numColumns={2}
            keyExtractor={item => item.merchandise_id}
            renderItem={item => {
              console.log(">>>", item);
              return (
                <View style={{ flex: 0.5 }}>
                  <View
                    style={{
                      flex: 1,
                      // borderColor: "black",
                      // borderWidth: 1,
                      margin: 1
                    }}
                  >
                    <ImageBackground
                      style={{
                        minHeight: 160,
                        width: null,
                        justifyContent: "flex-end"
                      }}
                      source={{ uri: item.item.image }}
                    >
                      <View
                        style={{
                          // backgroundColor: "rgba(0,0,0,0.5)",
                          minHeight: 50,
                          borderBottomLeftRadius: 10,
                          borderBottomRightRadius: 10
                        }}
                      >
                        <Text
                          style={{
                            color: "white",
                            marginLeft: 15,
                            fontSize: 10,
                            marginTop: 10
                          }}
                        >
                          {item.item.description}
                        </Text>
                      </View>
                    </ImageBackground>
                  </View>
                </View>
              );
            }}
          />
        </View>
    );
  }
}

export default Merchandise;
