import React from "react";
import { commonStyles } from "./styles";
import { Segment, Button, Form, CheckBox, DatePicker } from "native-base";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Alert,
  ImageBackground,
  KeyboardAvoidingView,
  ScrollView,
  Picker,AsyncStorage
} from "react-native";
import {
  Header,
  Card,
  CardItem,
  Thumbnail,
  Icon,
  Left,
  Body,
  Right,
} from "native-base";
// import { Header } from "react-navigation";
import BookStall from "../components/BookStall";
import background from '../assets/images/artWork.jpeg';
import { Constants, Facebook, Expo, Google } from "expo";
import google from "../assets/images/search.png";
import facebook from "../assets/images/facebook.png";
import { Dropdown } from "react-native-material-dropdown";
import axios from "axios";
import { apiUrl } from "../config/api";
import _isEmpty from 'lodash/isEmpty';
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment';
import { WP } from "../responsive";

class Book extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTime: 'Time',
      chosenDate: new Date(),
      isDateTimePickerVisible: false,
      brand: "",
      phone: "",
      email: "",
      Date: "",
      Time: "",
      events: [],
      location: null,
      stalls: null,
      selectedEvent: '',
      selectedLocation: '',
      selectedStall: '',
      language:"",
      dates:[{label:"Select Date",value:"Select Date"}],
      id:""
    };
    this.getDates()
  }

  setDate = newDate => {
    this.setState({ chosenDate: newDate });
  };

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
    var selectedTime = moment(date).format('hh:mm')
    this.setState({selectedTime});
    this.hideDateTimePicker();
  };
  getDates(){
    let state = this;
    axios
      .get(apiUrl + "/getalldates")
      .then((response)=> {
        state.setState({ dates: response.data });
      })
      .catch(function(error) {
        Alert.alert("Alert", "Bad request error.");
      });

  }
  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@MySuperStore:key');
      if(value){
          let newProduct = JSON.parse(value);
          this.setState({
            id:newProduct.userid

          })



      }
      else {

     
      }

    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
 

  componentDidMount(){
    this.getKey()
    // axios
    // .get(apiUrl + "/getlocation")
    // .then(function(response) {
    //   // console.log('location', response.data);
    //   this.setState({ location: response.data});
    // })
    // .catch(function(error) {
    //   Alert.alert("Alert", "Bad request error.");
    // });

    fetch(apiUrl + '/getschedule')
        .then((response) => response.json())
        .then((responseJson) => {
          for(var i=0; i < responseJson.length; i++){
            // console.log("e", events[i])
            const scheduleEvent = responseJson[i].event;
           scheduleEvent.map((e, index) => {
             this.setState({events: this.state.events.concat(e.event)})
            //  this.state.events.push(e.event)
           });
          }
          // this.setState({events: responseJson})
        })
        .catch((error) => {
          Alert.alert("Alert", JSON.stringify(error));
        });

    fetch(apiUrl + '/getlocation')
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({location: responseJson})
        })
        .catch((error) => {
          Alert.alert("Alert", JSON.stringify(error));
        });

    fetch(apiUrl + '/getstall')
    .then((response) => response.json())
    .then((responseJson) => {
      this.setState({stalls: responseJson})
    })
    .catch((error) => {
      Alert.alert("Alert", JSON.stringify(error));
    });
  }

  onSubmit = () => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var phoneno = /^\d+$/;
    if (!phoneno.test(this.state.phone)) {
      Alert.alert("Alert", "Fields empty.");
    } else if (!re.test(this.state.email)) {
      Alert.alert("Alert", "Email address is empty or invalid.");
    } else {
      console.log("[booking stalls]",this.state)
      let selectedStall = this.state.stalls.find(selectedStall => selectedStall.stall == this.state.brand);
      let selectedLocation = this.state.location.find(selectedLocation => selectedLocation.location == this.state.selectedLocation);
      // let events = this.state.events.find(obj => obj.location == this.state.selectedLocation);
      axios
        .get(apiUrl + "/bookingstall", {
          params: {
            booking_id: Math.random(),
            location_id:selectedLocation.location_id,
            stall_id:selectedStall.stall_id,
            event_name:"Coke Fest",
            brand:Math.random(),
            phone_number:this.state.phone,
            date:this.state.language,
            email:this.state.email,
            user_id:this.state.id

            // stall_id: this.state.selectedStall,
            // event_id: this.state.selectedEvent,
            // date: this.state.brand,
            // phone_number: this.state.phone,
            // email: this.state.email,
            // user_id: 2
          }
        })
        .then((response)=> {
          console.log("[response]",response)
          Alert.alert("Alert", "Request received. Stall booking will be confirmed after making advance payment.");
          this.props.navigation.navigate('DashboardScreen');
        })
        .catch((error) =>{
          console.log("show error",error)

          Alert.alert("Alert", "Error in booking stall.");
        });
    }
  };

  render() {
    const { seg, events, location, stalls, selectedTime} = this.state;
    var { height, width } = Dimensions.get("window");

  let eventsItems = (!_isEmpty(events)) ? events.map( (s, i) => {
    return <Picker.Item key={i} value={s} label={s} />
}) : <Picker.Item value={"2"} label={"2"}/>;

    let locationItems = (!_isEmpty(location)) ? location.map( (s, i) => {
      return <Picker.Item key={i} value={s.location} label={s.location} />
  }) : <Picker.Item value={"2"} label={"2"}/>;

  let stallsItems = (!_isEmpty(stalls)) ? stalls.map( (s, i) => {
    return <Picker.Item key={i} value={s.stall} label={s.stall} />
}) : <Picker.Item value={"2"} label={"2"}/>;

    return (
      <ScrollView contentContainerStyle = {{flexGrow:1}}>

      <ImageBackground style={{ flex: 1 }} source={background}>
      <Header
          hasSegment
          style={[{ backgroundColor: "#E52526", marginTop: 22,height:WP('20')  }]}
        >
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialIcons"
                name="arrow-back"
                style={[{ color: "white" }]}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={[{ color: "white", alignSelf: "flex-start", fontSize: 18 }]}
            >
              Book Your Stall
            </Text>
          </Body>
          <Right></Right>
        </Header>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior="padding"
        >
            <View style={{ height: height * 0.08 }} />
            <View style={{justifyContent:'center', alignItems:'center'}}>
     
            
          
            <View
              style={{
                backgroundColor: "white",
                color: "black",
                width: 300,
                borderColor: "black",
                borderWidth: 0.5,
                marginTop: 10
              }}
            >
              <Picker
                selectedValue={this.state.selectedLocation}
                style={{ height: 40, width: 300, color: "#656D68" }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ selectedLocation: itemValue })
                }
              >
                <Picker.Item label="Select Location" value="" />
                {/* <Picker.Item label="2" value="js" /> */}
                {locationItems}
              </Picker>
            </View>
            <View
              style={{
                backgroundColor: "white",
                color: "black",
               width: 300,
                borderColor: "black",
                borderWidth: 0.5,
                marginTop: 10
              }}
            >
              <Picker
                selectedValue={this.state.brand}
                style={{ height: 40, width:300, color: "#656D68" }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ brand: itemValue })
                }
              >
                <Picker.Item label="Select Stall" value="" />
                {/* <Picker.Item label="2" value="js" /> */}
                {stallsItems}
              </Picker>
            </View>
            <View
            style={{
              backgroundColor: "white",
              color: "black",
              width: 300,
              height:height*0.06,
              paddingVertical: 4,
              marginTop: 10,
              paddingRight: 10,
              borderColor: "black",
              borderWidth: 0.5,
            }}
          >
            {/* <DatePicker
              defaultDate={new Date(2019, 9, 6)}
              minimumDate={new Date(2019, 1, 1)}
              maximumDate={new Date(2019, 12, 31)}
              locale={"en"}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={"fade"}
              androidMode={"default"}
              placeHolderText="Date"
              textStyle={{ color: "grey",bottom:6,right:8 }}
              placeHolderTextStyle={{ color: "grey",bottom:6,right:8 }}
              onDateChange={this.setDate}
              disabled={false}
            /> */}
               <Picker
                selectedValue={this.state.language}
                style={{ height: 40, width:300, color: "#656D68" }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ language: itemValue })
                }
              >
                              <Picker.Item label="Select Date" value="java" />


{
  this.state.dates.map( (v)=>{
   return <Picker.Item label={v.label} value={v.value} />
  })
 }
                {/* <Picker.Item label="2" value="js" /> */}
              </Picker>
       
          </View>
            {/* <BookStall
              style={{ marginTop: 10 }}
              placeholder="Brand"
              autoCapitalize="none"
              value={this.state.brand}
              onChangeText={brand => this.setState({ brand })}
            /> */}
              {/* <BookStall
              style={{ marginTop: 10 }}
              placeholder="Event Name"
              autoCapitalize="none"
              value={this.state.selectedEvent}
              keyboardType={'email-address'}

              onChangeText={text => this.setState({ selectedEvent:text })}
            /> */}
            <BookStall
              style={{ marginTop: 10 }}
              placeholder="03xxxxxxxxx"
              autoCapitalize="none"
              value={this.state.phone}
              keyboardType={'phone-pad'}

              onChangeText={phone => this.setState({ phone })}
            />
            <BookStall
              style={{ marginTop: 10 }}
              placeholder="Email"
              autoCapitalize="none"
              value={this.state.email}
              keyboardType={'email-address'}

              onChangeText={email => this.setState({ email })}
            />

            {/* <BookStall
              style={{ marginTop: 10 }}
              placeholder="Date"
              autoCapitalize="none"
              value={this.state.Date}
              onChangeText={Date => this.setState({ Date })}
            /> */}
            {/* <TouchableOpacity  onPress={this.showDateTimePicker}>
             <View
            style={{
              backgroundColor: "white",
              color: "black",
              width: 300,
              height:height*0.06,
              paddingVertical: 4,
              paddingLeft: 10,
              paddingBottom: 4,
              marginTop: 10,
              paddingRight: 10,
              borderColor: "black",
              borderWidth: 0.5,
            }}
          >
            <Text style={{color:"gray",marginTop:4,fontSize:16}}>{selectedTime}</Text>
              <DateTimePicker 
              mode="time"
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />
        </View>
        </TouchableOpacity> */}
            {/* <BookStall
              style={{ marginTop: 10 }}
              placeholder="Time"
              autoCapitalize="none"
              value={this.state.Time}
              onChangeText={Time => this.setState({ Time })}
            /> */}
          </View>

            <TouchableOpacity onPress={this.onSubmit}>
              <View
                style={[
                  {
                    backgroundColor: "#df1d25",
                    width: width * 0.7,
                    alignItems: "center",
                    width: 300,
                    paddingVertical: 8,
                    marginBottom: 5,
                    alignSelf: "center",
                    marginTop: 20
                  }
                ]}
              >
                <Text
                  style={[
                    {
                      fontSize: 18,
                      textAlign: "center",
                      color: "white",
                      fontWeight: "bold"
                    }
                  ]}
                >
                  Book Now
                </Text>
              </View>
            </TouchableOpacity>
            <Text
              style={{
                alignSelf: "center",
                width: width * 0.65,
                marginTop: 10,
                textAlign: "center"
              }}
            >
              Disclamer: Stall Booking will be confirmed after making advance payment.
            </Text>
            
        </KeyboardAvoidingView>
      </ImageBackground>
      </ScrollView>

    );
  }
}

export default Book;
