import React, { Component } from "react";
import {
  Image,
  View,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  Dimensions,Alert,FlatList,Picker
} from "react-native";
import axios from "axios";
import { apiUrl } from "../config/api";
import RNPickerSelect from 'react-native-picker-select';
import background from '../assets/images/artWork.jpeg';

import moment from 'moment'
import {
  Header,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  DatePicker
} from "native-base";
import BookStall from "../components/BookStall";
import { WP } from "../responsive";

class Schedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chosenDate: new Date(),
      chosenEvents:[],
      dates:[],
      tablePng:""
     
    };
  }

  componentWillMount() {
    let state = this;
    axios
      .get(apiUrl + "/getscheduleimage")
      .then((response)=> {
        console.log("i am sceduling",response.data[0].image)
        // state.setState({ dates: response.data });
        if(response.data){
          this.setState({tablePng:response.data[0].image})


        }
      })
      .catch(function(error) {
        Alert.alert("Alert", "Bad request error.");
      });
  }



  show(newDate){
    let state = this;

    console.log("selected date",newDate)
    dateTime = moment(newDate).format("MM/DD/YYYY");
    console.log("converted date",dateTime)
    axios
    .get(apiUrl + "/getschedule", {
      params: {
        date:dateTime
      }
    })
    .then((response)=> {

      console.log(">>>", response.data);
        this.setState({chosenEvents:response.data})
 
    })
    .catch((error) =>{
      console.log("show eror",error)

      Alert.alert("Alert", "Bad request error.");
    });
    
  }
 
  render() {
    var { height, width } = Dimensions.get("window");
    return (
      <ImageBackground style={{ flex: 1, }}
      source={background}      >
              <Header
          hasSegment
          style={[{ backgroundColor: "#E52526", marginTop: 22,height:WP('20')  }]}
        >
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialIcons"
                name="arrow-back"
                style={[{ color: "white" }]}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={[{ color: "white", alignSelf: "flex-start", fontSize: 18 }]}
            >
            Schedule
            </Text>
          </Body>
          <Right></Right>
        </Header>

        <View style = {{display:'flex',height:"100%",width:"100%"}}>
        <Image
          source={{uri:this.state.tablePng}}
          style={{ width: WP('100'), height: WP('100'),resizeMode:'center'}}
        />
     

        

        </View>
        

     
     
     
    
      </ImageBackground>
    );
  }
}
export default Schedule;

