import React, { Component } from 'react';
import { StyleSheet, ActivityIndicator, View,TouchableHighlight,Text,TouchableOpacity } from 'react-native';
import { WebView } from "react-native-webview";
import { WP } from '../responsive';
import {

  Icon,

} from "native-base";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
export default class FullScreenVideo extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: true };
  }

  showSpinner() {
    console.log('Show Spinner');
    this.setState({ visible: true });
  }

  hideSpinner() {
    console.log('Hide Spinner');
    this.setState({ visible: false });
  }

  render() {
      console.log("show props",this.props.navigation)
    return (
      <View
        style={this.state.visible === true ? styles.stylOld : styles.styleNew}>
        {this.state.visible ? (
          <ActivityIndicator
            color="red"
            size="large"
            style={styles.ActivityIndicatorStyle}
          />
        ) : null}

        <WebView
          style={styles.WebViewStyle}
          //Loading URL
          source={{ uri:this.props.navigation.state.params.params }}
          //Enable Javascript support
          //For the Cache
          //View to show while loading the webpage
          //Want to show the view or not
          //startInLoadingState={true}
          onLoadStart={() => this.showSpinner()}
          onLoad={() => this.hideSpinner()}
        />
         <TouchableOpacity style={{position:'absolute',bottom:WP('10'),left:WP('5'),zIndex:1,height:WP('15'),width:WP('15'),backgroundColor:'red',borderRadius:100,alignItems:'center',justifyContent:'center'}}
         onPress = {()=>this.props.navigation.goBack()}
         >
                               
                   <Icon
    type="MaterialIcons"
    name="arrow-back"
    style={[
      { color: "black", fontSize: 28, }
    ]}
  />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  stylOld: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  styleNew: {
    flex: 1,
  },
  WebViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop: 40,
  },
  ActivityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
});
