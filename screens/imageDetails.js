import React from 'react';
import {  Header, Left, Body, Right,Button,Icon } from 'native-base';
import {
    View,
    Text,
    Dimensions,
ImageBackground,
StyleSheet,Image
} from 'react-native';
import { WP } from '../responsive';


export default class ImageDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        var { height, width } = Dimensions.get('window');
        console.log("[image details ]",this.props.navigation.state.params.images)

        return (
                <View style = {{flex:1}}>
                <Header
          hasSegment
          style={[{ backgroundColor: "#E52526", marginTop: 22,height:WP('20')  }]}
        >
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialIcons"
                name="arrow-back"
                style={[{ color: "white" }]}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={[{ color: "white", alignSelf: "flex-start", fontSize: 18 }]}
            >
                Festival Map
            </Text>
          </Body>
          <Right></Right>
        </Header>
        <View style = {{display:'flex',height:"90%",width:"100%"}}>

        {
            this.props.navigation.state.params.images ?
            (this.props.navigation.state.params.images.backimage)
            ?
            <Image
            source = {{uri:this.props.navigation.state.params.images.backimage}}
            style = {{width:"100%",height:"100%"}}
            resizeMode = {'contain'}
            />
            :
            <Image
            source = {{uri:`https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQja-rJ0yivWBY_ft6m5udRF3T6H6VqVfMrtR2aYFctM74YmUyQ`
            }}
            style = {{width:"100%",height:"100%"}}
            resizeMode = {"contain"}
            />
            :
            <Image
            source = {{uri:`https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQja-rJ0yivWBY_ft6m5udRF3T6H6VqVfMrtR2aYFctM74YmUyQ`
            }}
            style = {{width:"100%",height:"100%"}}
            resizeMode = {"contain"}
            />
           

        }


        </View>

          </View>
           
              
             
    


        );
    }
}



var styles = StyleSheet.create({
    column: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        width: 200
    },
    row: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        flexWrap: 'wrap',
        flex: 1
    },
    bullet: {
        width:10,
        fontSize:30,
        fontWeight:"bold"
    },
    bulletText: {
        flex: 1
    },
    boldText: {
        fontWeight: 'bold'
    },
    normalText: {
    }
});