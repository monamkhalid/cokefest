import React from 'react';
import {
  Dimensions,
  ImageBackground,AsyncStorage,View
} from 'react-native';

import { commonStyles } from './styles';
import Background from '../assets/images/popup.png';
import { StackActions, NavigationActions } from 'react-navigation';
import { Video } from 'expo-av';
// import { Video } from 'expo';



export default class Welcome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.getKey()
  }

  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@MySuperStore:key');
      if(value){
        console.log("store start",value)
        setTimeout(() => {
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'WelcomeScreen2' })],
            // actions: [NavigationActions.navigate({ routeName: 'SignInScreen' })],

          });
          this.props.navigation.dispatch(resetAction);


        }, 9000)
       






      }
      else {
        // this.props.navigation.push('SignInScreen');
        console.log("store start",value)

      
        setTimeout(() => {
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'SignInScreen' })],
            // actions: [NavigationActions.navigate({ routeName: 'SignInScreen' })],
  
          });
          this.props.navigation.dispatch(resetAction);


        }, 9000)

     
      }

    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }
  

  render() {
    var { height, width } = Dimensions.get('window');
    return (
      // <ImageBackground
      //   style={{ width: '100%', height: '100%' }}
      //   source={Background}
      // />
      <View style = {{flex:1,backgroundColor:"#fff",alignItems:'center',justifyContent:'center'}}>
            <Video
  source={require('../assets/images/splashapp.mp4')}
  rate={1.0}
  volume={1.0}
  isMuted={false}
  resizeMode = {"contain"}
  shouldPlay
  // isLooping

  
  style = {{display:"flex",height:"100%",width:"100%"}}
/>
        
      </View>

    );
  }
}