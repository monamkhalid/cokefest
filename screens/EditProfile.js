import React from 'react';
import { commonStyles } from './styles';
import { Segment, Button, Form, Thumbnail,Icon } from 'native-base';
import {
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    Alert,
    StatusBar,
    Image,
    ImageBackground,
    KeyboardAvoidingView,AsyncStorage,
    ScrollView
} from 'react-native';
import { Header } from 'react-navigation';

import InputText from '../components/InputText';
import icon from '../assets/images/icon.png';
import background from '../assets/images/artWork.jpeg';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import swiper1 from '../assets/images/Swiper1.jpg';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { apiUrl } from "../config/api";
import axios from "axios";
import {WP,HP} from '../responsive';
import { StackActions, NavigationActions } from 'react-navigation';


class EditProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id:"",
            name: '',
            last:'',
            phone: '',
            email: '',
            password: '',
            image: null,

        };
    }
    clearStore(){

  AsyncStorage.clear();

}
    async getKey() {
        try {
          const value = await AsyncStorage.getItem('@MySuperStore:key');
          if(value){
              let newProduct = JSON.parse(value);
              this.setState({
                name: newProduct.name,
                last:newProduct.lastname,
                phone:newProduct.phoneno,
                email:newProduct.useremail,
                image:newProduct.profilepic

              })
    
    
    
          }
          else {
    
         
          }
    
        } catch (error) {
          console.log("Error retrieving data" + error);
        }
      }
      componentDidMount(){
        this.getKey()

      }
      async saveKey(value) {
        try {
          await AsyncStorage.setItem('@MySuperStore:key', value);
        } catch (error) {
          console.log("Error saving data" + error);
        }
      }

    onSubmit = () => {

      if(this.state.name && this.state.last){



        var phoneno = /^\d+$/;
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (this.state.name.match(/\d+/g) || this.state.name.length < 3) {
                Alert.alert(
                    "Alert",
                    "Name must be 3-25 characters long. Do not enter numbers."
                );
            }

           else if (this.state.last.match(/\d+/g) || this.state.last.length < 2) {
                Alert.alert(
                    "Alert",
                    "Last Name must be 2-25 characters long. Do not enter numbers."
                );
            }

 else {
            axios
            .get(apiUrl + "/edit_user_profile", {
              params: {
                user_id:this.state.id,
                username:"gren",
                firstname:this.state.name,
                lastname:this.state.last,
                image:"done"

              }
            })
            .then((response)=> {
                console.log("response",response)
              if (response.data.result == "false") {
                Alert.alert("Alert", "Wrong Code, try again.");
              } else {
                // navigate("SignInScreen", { user_id: response.data.user_id });
                var token = {
                  userid:this.state.id,
                  name: `${this.state.name}${this.state.last}`,
                  phoneno:this.state.phone,
                  lastname:this.state.last,
                  useremail:this.state.email,
                  profilepic:this.state.image
                }
                this.saveKey(JSON.stringify(token))
                // console.log("code response",response)
                // navigate("WelcomeScreen2");
                Alert.alert("Alert",response.data.message);

    
    
              }
            })
            .catch((error)=> {
              console.log("app dev",error)
              Alert.alert("Alert", "Bad request error.");
            });
        }
      }
      else{
        Alert.alert("Alert", "Fields Empty");
      }
    };
    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
          const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
          if (status !== 'granted') {
            alert('Sorry, we need camera roll permissions to make this work!');
          }
        }
      }
      _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: false,
          aspect: [4, 3],
        });
    
        console.log("grey",result);
    
        if (!result.cancelled) {
          this.setState({ image: result.uri });
          console.log("grey",result);

        }
      };

    render() {
        const { seg } = this.state;
        var { height, width } = Dimensions.get('window');
        let { image } = this.state;


        return (

            <ImageBackground style={{ flex: 1 }} source={background}>
               <View style = {{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between',backgroundColor: "#E52526",height:WP('20'),marginTop:22}}>
               <Button style={{alignSelf:"center"}}
                            transparent
                            onPress={() => {
                                this.props.navigation.goBack()
                                }}
                        >
                            <Icon
                                type="MaterialIcons"
                                name="arrow-back"
                                style={[commonStyles.fontSize18, { color: "white" }]}
                            />
                        </Button>
                        <TouchableOpacity style = {{padding:10}}
                        onPress = {()=>{
                            this.clearStore()
                            const resetAction = StackActions.reset({
                                index: 0,
                                actions: [NavigationActions.navigate({ routeName: 'SignInScreen' })],
                                // actions: [NavigationActions.navigate({ routeName: 'SignInScreen' })],
                    
                              });
                              this.props.navigation.dispatch(resetAction);

                        }}
                        >
                            <Text style = {{color:"#fff",fontWeight:'bold'}}>Sign-Out</Text>
                        </TouchableOpacity>

               </View>
                      
                  
                  
             
                <KeyboardAvoidingView style={{ flex: 1 }} keyboardVerticalOffset={Header.HEIGHT + 90} behavior="padding">
                    <ScrollView>
                        <View style={{ height: height * 0.06 }} />
                        <View style={{ flex: 1, }}>
                        <View style = {{display:'flex',height:WP('25'),width:WP('25'),borderRadius:100,alignSelf:'center',backgroundColor:'red',position:'relative', zIndex: 1,}}>
                        <View style = {{display:'flex',width:"100%",height:"100%",overflow:'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 60,}}>
                           
                             <Image 
          resizeMode = "cover"
          source={{ uri: image }} style={{ width: "100%", height: "100%" }} />

                        </View>
                        <TouchableOpacity style = {{display:'flex',bottom: WP('0'),position: 'absolute',right:WP(-2)}}
                                                    onPress={this._pickImage}

                        >
                        <Image
                        source = {require('../assets/images/camera.png')}
                        style = {{height:WP('10'),width:WP('10')}}
                        resizeMode = "contain"
                        
                        />

                        </TouchableOpacity>

                        </View>
    
                        <Text style={{alignSelf:"center",marginTop:15,fontWeight:'bold',color:'gray',fontSize:16}}>{this.state.name} {this.state.last}</Text>
                            <Form style={{marginTop:20}}>
                                <InputText
                                    icon="person-outline"
                                    placeholder="First Name(Minimum 3 Charaters)"
                                    onChangeText={text => this.setState({ name: text })}
                                    value={this.state.name}
                                />
                                 <InputText
                                    icon="person-outline"
                                    placeholder="Last Name"
                                    onChangeText={text => this.setState({ last: text })}
                                    value={this.state.last}
                                />
                                <InputText
                                editable={false}
                                    icon="call"
                                    placeholder="Phone Number"
                                    onChangeText={text => this.setState({ phone: text })}
                                    value={this.state.phone}
                                />
                                <InputText
                                  editable={false}
                                    icon="mail-outline"
                                    placeholder="Email"
                                    onChangeText={text => this.setState({ email: text })}
                                    value={this.state.email}
                                />

                                {/* <InputText
                                    icon="lock-outline"
                                    placeholder=" Enter Your Password"
                                    autoCapitalize="none"
                                    value={this.state.password}
                                    secureTextEntry={true}
                                    onChangeText={password => this.setState({ password })}
                                /> */}

                            </Form>

                            <View style={{ marginTop: 60,flexDirection:"row",alignSelf:"center", }}>
                                <TouchableOpacity
                                onPress={this.onSubmit}
                                >
                                    <View
                                        style={[

                                            {
                                               display:'flex',
                                               backgroundColor: '#df1d25',
                                               width: WP('30'),
                                               alignItems: 'center',
                                               height: WP('10'),
                                               justifyContent:'center'
                                               
                                            }
                                        ]}
                                    >

                                        <Text
                                            style={[
                                                {
                                                    fontSize: 18, textAlign: "center",
                                                    color: "white", fontWeight: 'bold'
                                                },
                                            ]}
                                        >
                                            Save
                    </Text>
                                    </View>
                                </TouchableOpacity>
                                {/* <TouchableOpacity onPress={this.onSubmit}>
                                    <View
                                        style={[

                                            {
                                                backgroundColor: '#df1d25',
                                                width: width * 0.35,
                                                alignItems: 'center',
                                                paddingVertical: 8,
                                                marginBottom: 5,
                                                alignSelf: 'center',
                                                marginLeft:20
                                            }
                                        ]}
                                    >

                                        <Text
                                            style={[
                                                {
                                                    fontSize: 18, textAlign: "center",
                                                    color: "white", fontWeight: 'bold'
                                                },
                                            ]}
                                        >
                                            Submit
                    </Text>
                                    </View>
                                </TouchableOpacity> */}
                            </View>
                        </View>
                    </ScrollView>
                </KeyboardAvoidingView>
            </ImageBackground>
        );
    }
}

export default EditProfile;
