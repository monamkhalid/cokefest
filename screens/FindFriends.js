import React from 'react';
import { commonStyles } from './styles';
import { Button, Header, Item, Input, Icon, List, ListItem, Thumbnail, Left, Body, Right } from 'native-base';
import {
    View,
    Text,
    Dimensions,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import Swiper1 from '../assets/images/Swiper1.jpg';


class Findfriends extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [{
                name: 'Sankhadeep',
                text: 'I m avalible',
                button: "accept"

            },
            {
                name: 'Ali',
                text: 'I m avalible',
                button: "accept"

            },
            {
                name: 'usman',
                text: 'I m avalible',
                button: "invite"

            },
            {
                name: 'alex',
                text: 'I m avalible',
                button: "accept"

            },
            {
                name: 'Ali',
                text: 'I m avalible',
                button: "invite"

            },
            {
                name: 'usman',
                text: 'I m avalible',
                button: "accept"

            },
            {
                name: 'alex',
                text: 'I m avalible',
                button: "invite"

            },
            {
                name: 'Ali',
                text: 'I m avalible',
                button: "accept"

            },
            {
                name: 'usman',
                text: 'I m avalible',
                button: "invite"

            },
            {
                name: 'alex',
                text: 'I m avalible',
                button: "accept"

            },

            ]
        };
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <Header searchBar style={{ marginTop: 22 ,backgroundColor:"#E52526" }}>
                    <Left style={{ flex: null }}>
                        <Button
                            transparent
                            onPress={() => {
                                this.props.navigation.openDrawer()
                                }}
                        >
                            <Icon
                                type="MaterialIcons"
                                name="menu"
                                style={[commonStyles.fontSize18, { color: "white",marginRight:5 }]}
                            />
                        </Button>
                    </Left>

                    <Item style={{height: 30}} >
                        <Input placeholder="Find your friend" style={{ marginLeft: 5,fontSize:14 }} />
                    </Item>
                </Header>
                <View>
                    <Text style={{marginTop:20,marginLeft:20,fontWeight:"bold",fontSize:16}}>
                        Find find friends
                    </Text>
                    <ScrollView>
                    {this.state.list.map((l, i) => (
                    <List style={{marginTop:5}} noBorder key={i}>
            <ListItem thumbnail noBorder >
              <Left>
                <Thumbnail  source={Swiper1}  />
              </Left>
              <Body>
                <Text>{l.name}</Text>
                <Text style={{color:"#A1A4A3",fontSize:12}}>{l.text}</Text>
              </Body>
              <Right>
                <Button transparent>
                <TouchableOpacity onPress={this.signIn} style={{ marginLeft: 10 }}>
              <View
                style={[

                  {
                    backgroundColor: 'grey',
                    width: width * 0.25,
                    alignItems: 'center',
                    paddingVertical: 4,
                    marginBottom: 5,
                    alignSelf: 'center'
                  }
                ]}
              >

             <Text>{l.button}</Text>
              </View>
            </TouchableOpacity>
                </Button>
              </Right>
            </ListItem>
          </List>
))}
                    </ScrollView>
                </View>


             
            </View>


        );
    }
}

export default Findfriends;


