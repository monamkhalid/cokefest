import React from 'react';
import {  Header, Left, Body, Right,Button,Icon } from 'native-base';
import {
    View,
    Text,
    Dimensions,
ImageBackground,
    TouchableOpacity,
    StyleSheet,Image,ScrollView
} from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Swiper1 from '../assets/images/Swiper1.jpg';
import Welcome from '../assets/images/welcome-1.jpg';
import Welcome1 from '../assets/images/Welcome1.png';
import Welcome2 from '../assets/images/Welcome2.png';
import Carousel,{Pagination,ParallaxImage} from 'react-native-snap-carousel'; // 3.6.0
import{WP,HP} from '../responsive';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImageResizeMode from 'react-native/Libraries/Image/ImageResizeMode'

class HelpScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          images: [
            {
              id: "sNPnbI1arSE",
              thumbnail:Welcome1,
              title: "Eminem - My Name Is"
            }, {
              id: "Sdadsadaads",
              thumbnail:Welcome2 ,
              title: ""
            }
          ],
          activeSlide:0
        }
    }
    handleSnapToItem(index){
      console.log("snapped to ", index)
      this.setState({ activeSlide: index })
    }
    _renderItem = ( {item, index} ) => {
      console.log("rendering,", index, item)
      return (
        this.state.activeSlide == 1 ?
        <TouchableOpacity onPress = {()=>{ this.props.navigation.navigate('DashboardScreen')}}
        activeOpacity={0.9}

        >
 <Image
 style={{ width: "100%", height: "100%" }}
  //  resizeMode={"contain"}
        source = {item.thumbnail}
        />
        </TouchableOpacity>
        :
   
        <Image
        style={{ width: "100%", height: "100%" }}
// resizeMode={"contain"}
        source = {item.thumbnail}
        />
      );
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
         
            <ScrollView contentContainerStyle = {{flexGrow:1}}>
            <ImageBackground style={{ flex: 1 }} source={Welcome}>
                <View style={{paddingTop:50,   alignItems: 'flex-end',display:'flex'}}>
                 
                <Button transparent onPress={() => {
              this.props.navigation.navigate('EditProfile');
            }}>
                            <Icon
                                type="MaterialIcons"
                                name="person-outline"
                                style={[{ color: "black",fontSize:25,justifyContent:"flex-end",marginLeft:WP('13') }]}
                            />
                        </Button>
                       
                </View>
                <View style = {{display:'flex',alignItems:'center',justifyContent:'center',height:WP('10')}}>
              

                </View>
                <View style = {{display:'flex',alignItems:'center',justifyContent:'center',height:WP('147')}}>
                <Carousel
          ref={ (c) => { this._carousel = c; } }
          data={this.state.images}
          renderItem={this._renderItem.bind(this)}
          onSnapToItem={this.handleSnapToItem.bind(this)}
          sliderWidth={width}
          itemWidth={width}
          itemHeight = {180}
          layout={'default'}
          firstItem={0}
        
        />
           <Pagination
           carouselRef = {this._carousel}
                  dotsLength={this.state.images.length}
                  activeDotIndex={this.state.activeSlide}
                  containerStyle={{display:'flex'}}
                  dotColor={'#000'}
                  // dotStyle={styles.paginationDot}
                  inactiveDotColor={"#000"}
                  inactiveDotOpacity={0.4}
                  inactiveDotScale={0.6}
              
                />

                </View>
      

        <TouchableOpacity
           style={[

            {
              backgroundColor: '#df1d25',
              width: width * 0.4,
              alignItems: 'center',
              paddingVertical: 8,
              marginBottom: 5,
              alignSelf: 'center',
              marginBottom:WP('5')
              // bottom:30
            }
          ]}
        onPress={() => {
              this.props.navigation.navigate('DashboardScreen');
              
            }} >
         
             
    

                <Text
                  style={[
                    {
                      fontSize: 18, textAlign: "center",
                      color: "white", fontWeight: 'bold'
                    },
                  ]}
                >
                  EXPLORE
                    </Text>
            </TouchableOpacity>
               
      </ImageBackground>
      </ScrollView>
              
             
    


        );
    }
}

export default HelpScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  child: {
    justifyContent:"center",
    alignItems:"center",
    margin:10,
  
    justifyContent: 'center'
  },
  text: {
    fontSize: 10,
    textAlign: 'center'
  }
});
