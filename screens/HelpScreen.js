import React from 'react';
import {  Header, Left, Body, Right,Button,Icon } from 'native-base';
import {
    View,
    Text,
    Dimensions,
ImageBackground,
    Image,
    TouchableOpacity,
    Linking,
    ScrollView,BackHandler
} from 'react-native';
import background from '../assets/images/artWork.jpeg';
import { WP } from '../responsive';




class HelpScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    // componentDidMount() {
    //   this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    // }
  
    // componentWillUnmount() {
    //   this.backHandler.remove()
    // }
  
    // handleBackPress = () => {
    //   BackHandler.exitApp()
      
    // }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <ImageBackground style={{ width: '100%', height: '100%',}} source={background}>
             
                           <Header
          hasSegment
          style={[{ backgroundColor: "#E52526", marginTop: 22,height:WP('20')  }]}
        >
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialIcons"
                name="arrow-back"
                style={[{ color: "white" }]}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={[{ color: "white", alignSelf: "flex-start", fontSize: 18 }]}
            >
            FAQs
            </Text>
          </Body>
          <Right></Right>
        </Header>
                        {/* <View style={{backgroundColor:'red',height:30,justifyContent:"center",}}>
                        <Text style={{marginLeft:20,color:'white'}}>FAQS</Text>
                        </View> */}
               
                        <ScrollView>
                          <View style = {{display:'flex',padding:WP('5')}}>

                      
                        <Text style={{fontSize:10,fontWeight:'bold'}}>Q1 How can I reach out/send specifications to relevant executives regarding my platform?  
        
 </Text>
 <Text style = {{fontSize:10,marginTop:5}}>
                          
 Thank you for reaching out to us. For stall bookings & registration kindly contact info@activemedia.com.pk or food.fest@activemedia.com.pk </Text> 
<Text style={{fontSize:10,marginTop:5,fontWeight:'bold'}}>
Q2. When will we be informed about stall finalization and relevant details?     
</Text>
<Text style = {{fontSize:10,marginTop:5}}>
You’ll be informed shortly after your request for stall booking. Our representative will contact you within in a day or two and will guide you accordingly. </Text> 

<Text  style={{fontSize:10,marginTop:5,fontWeight:'bold'}}>Q3. Will Tickets be available on-spot?  
</Text>
<Text style = {{fontSize:10,marginTop:5}}>
Yes, tickets will be available on-spot. A lot of fun and excitement awaits you. Looking forward to seeing you.
</Text>
<Text style = {{fontSize:10,marginTop:5,fontWeight:'bold'}}>
Q4. For e-ticket holders, how will they be assisted? Why is it better to purchase online?   
</Text>
<Text style = {{fontSize:10,marginTop:5}}>
For e-ticket holders, we have a separate booth that will be placed at the venue. Buy online and get rid of all the last minute hassle. Enjoy all the performances and food right on time.
</Text>
<Text style = {{fontSize:10,marginTop:5,fontWeight:'bold'}}>
Q5.  In case of emergency what is the protocol/ how can we reach out to the considered professionals?
</Text>
<Text style = {{fontSize:10,marginTop:5}}>
We care for our audience and we want you to have the time to your life without any inconvenience. In case of emergency you can reach us at the emergency booth which is right next to the entrance. </Text>
<Text style = {{fontSize:10,marginTop:5,fontWeight:'bold'}}>
Q6. Do you have a lost and found desk? 
</Text>
<Text style = {{fontSize:10,marginTop:5}}>
Lost something? Don’t worry, we got your back. You can reach us at the lost n found desk which is right next to the entrance. </Text>
<Text style = {{fontSize:10,marginTop:5,fontWeight:'bold'}}>
Q7. What measures have been taken to ensure safety at the festival?
</Text>
<Text style = {{fontSize:10,marginTop:5}}>
Every year we take measures to make your experience even better. This time, we have further enhanced security protocols, added multiple entrances & increased the number of help/emergency desks at the venue. </Text>
<Text style = {{fontSize:10,marginTop:5,fontWeight:'bold'}}>
Q8. When will the winners be announced? When will we receive our prizes? 
</Text>
<Text style = {{fontSize:10,marginTop:5}}>
We wish you all the best and we hope that you win exciting prizes. For details, kindly visit our facebook page. </Text>
<Text style = {{fontSize:10,marginTop:5,fontWeight:'bold'}}>
Q9. How to enter into competitions? How to win free tickets? 
</Text>
<Text style = {{fontSize:10,marginTop:5}}>


Visit our Facebook page, enter the competition and get a chance to win. So, what are you waiting for? </Text>



</View>
<TouchableOpacity onPress={() => Linking.openURL('mailto:info@activemedia.com.pk?subject=SendMail&body=Description') }
      title="support@example.com" >
              <View
                style={[

                  {
                    backgroundColor: '#df1d25',
                    width: width * 0.7,
                    alignItems: 'center',
                    width: 200,
                    paddingVertical: 8,
                    marginBottom: 5,
                    alignSelf: 'center',
                    marginTop:20,
                    marginBottom:WP('10')
                  }
                ]}
              >

                <Text
                  style={[
                    {
                      fontSize: 18, textAlign: "center",
                      color: "white", fontWeight: 'bold'
                    },
                  ]}
                >
                 Email Us 
                    </Text>
              </View>
            </TouchableOpacity>
            </ScrollView>
      </ImageBackground>
              
             
    


        );
    }
}

export default HelpScreen;


