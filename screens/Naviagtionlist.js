import React, { Component } from 'react';
import { StyleSheet, ActivityIndicator, View,Linking } from 'react-native';
import { WebView } from "react-native-webview";
import axios from "axios";
import { apiUrl } from "../config/api";

export default class MainActivity extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: true,mapUri:"" };
  }

  showSpinner() {
    console.log('Show Spinner');
    this.setState({ visible: true });
  }

  hideSpinner() {
    console.log('Hide Spinner');
    this.setState({ visible: false });
  }
  componentDidMount(){
    axios
    .get(apiUrl + "/getlocation")
    .then((response)=> {
      console.log("show links on maps",response)
      if(response.data){
        let obj = response.data.find(obj => obj.location_id == 1);
        console.log("filtered obj",obj)
        if(obj){
          if(obj.path){
                Linking.openURL(obj.path).catch((err) => console.error('An error occurred', err));


          }

        }


      }
    })
    .catch((error)=> {
      console.log("show links on maps",error)

    });

  }
  render() {
    return (
      <View style={styles.container}>
         {/* <View
          style={{ marginTop: 30, flexDirection: "row", alignItems: "center" }}
        >
          <Button
            transparent
            onPress={() => {
              this.props.navigation.openDrawer();
            }}
          >
            <Icon
              type="MaterialIcons"
              name="dehaze"
              style={[{ color: "black", fontSize: 30, marginLeft: 20 }]}
            />
          </Button>
          <Text style={{ marginLeft: 10, fontSize: 20 }}>Navigation</Text>
        </View> */}
      
        {/* <MapView
                showsUserLocation={true}

          style={{ alignSelf: 'stretch', height: "100%" }}
          region={{ latitude: this.state.location.coords.latitude, longitude: this.state.location.coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 }}
        >
     <MapView.Marker
      coordinate={this.state.location.coords}
      title="My Marker"
      description="Some description"
    /> 
    {this.state.markers.map(marker => (
      
    <MapView.Marker
    key = {Math.random()}
      coordinate={{latitude:marker.cordinates.lat != null ?parseFloat(marker.cordinates.lat):0.0,longitude:marker.cordinates.long!= null ?parseFloat(marker.cordinates.long) :0.0}}
      title={marker.name}
    />
  ))} 
        </MapView> */}
           {/* <WebView
          style={styles.WebViewStyle}
          //Loading URL
          source={{ uri: 'https://www.google.com/maps/dir//Lake+City,+Lahore,+Punjab,+Pakistan/@31.3538748,74.2350252,14z/data=!4m8!4m7!1m0!1m5!1m1!1s0x3919aa91e49da6af:0xc134fd8552b3464d!2m2!1d74.2560806!2d31.3543706' }}
          //Enable Javascript support
          javaScriptEnabled={true}
          //For the Cache
          domStorageEnabled={true}
          //View to show while loading the webpage
          //Want to show the view or not
          //startInLoadingState={true}
          onLoadStart={() => this.showSpinner()}
          onLoad={() => this.hideSpinner()}
        /> */}
      
      </View>
    );
  }
}

const styles = StyleSheet.create({
  stylOld: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  styleNew: {
    flex: 1,
  },
  WebViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop: 40,
  },
  ActivityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
});
