// import React from "react";
// import { commonStyles } from "./styles";
// import { Segment, Button, Form,DatePicker,Icon ,Picker,Item } from "native-base";
// import {
//   View,
//   Text,
//   Dimensions,
//   TouchableOpacity,
//   Alert,
//   StatusBar,
//   Image,
//   ImageBackground,
//   KeyboardAvoidingView,
//   ScrollView
// } from "react-native";
// import { Header } from "react-navigation";
// import { apiUrl } from "../config/api";
// import axios from "axios";

// import InputText from "../components/InputText";
// import background from '../assets/images/Buy-Your-Ticket.png';

// class BuyTicket extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       isDateTimePickerVisible: false,
//       name: "",
//       phone: "",
//       dateOfBirth: "",
//       tickets:"",
//       eventlocation:""
//     };
//   }

//   setDate = newDate => {
//     this.setState({ chosenDate: newDate });
//   };


//   onSubmit = () => {
//     var phoneno = /^\d+$/;
//     var date = /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/;
//     var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//     if (this.state.name.match(/\d+/g) || this.state.name.length < 3) {
//       Alert.alert(
//         "Alert",
//         "Name must be 3-25 characters long. Do not enter numbers."
//       );
//     } else if (this.state.name.match(/\d+/g) || this.state.name.length > 25) {
//       Alert.alert(
//         "Alert",
//         "Name cannot be longer than 25 characters. Do not enter numbers."
//       );
//     } else if (!phoneno.test(this.state.phone)) {
//       Alert.alert("Alert", "Mobile No is empty or invalid.");
//     } else if (!date.test(this.state.dateOfBirth)) {
//       Alert.alert("Alert", "Date of Birthday is empty or invalid.");
//     } else {
//       axios
//         .get(apiUrl + "/buyticket", {
//           params: {
//             name: this.state.name,
//             phone_number: this.state.phone,
//             event_id: 1,
//             location_id: 1,
//             user_id: 1,
//             datetime: this.state.dateOfBirth,
//             path: "easyticket"
//           }
//         })
//         .then(function(response) {
//           Alert.alert("Alert", "Successfull Buy ticket.");
//         })
//         .catch(function(error) {
//           Alert.alert("Alert", "Error in bookin ticket.");
//         });
//     }
//   };

//   render() {
//     const { seg } = this.state;
//     var { height, width } = Dimensions.get("window");

//     return (
//       <ScrollView>

//       <ImageBackground style={{ flex: 1 }} source={background}>
//         <KeyboardAvoidingView
//           style={{ flex: 1 }}
//           keyboardVerticalOffset={Header.HEIGHT + 90}
//           behavior="padding"
//         >
//             <View style={{ height: height * 0.3 }} />
//             <View style={{ flex: 1 }}>
//               <Text
//                 style={{
//                   color: "grey",
//                   fontSize: 20,
//                   marginLeft: 30,
//                   fontWeight: "bold"
//                 }}
//               >
//                 Buy your ticket
//               </Text>
//               <Form style={{ marginTop: 30 }}>
//                 <InputText
//                   icon="person-outline"
//                   placeholder="Name"
//                   autoCapitalize="none"
//                   value={this.state.name}
//                   onChangeText={name => this.setState({ name })}
//                 />
//                  {/* <InputText
//                   icon="location-on"
//                   placeholder="Location"
//                   autoCapitalize="none"
//                   value={this.state.eventlocation}
//                   onChangeText={name => this.setState({ eventlocation:name })}
//                 /> */}
//                    <View style={{ marginLeft: 20, marginRight: 20,marginTop:"5%" }}>
//       <Item style={{ borderColor: "grey" }}>
//         <Icon
//           type="MaterialIcons"
//           name="location-on"
//           style={[{ fontSize: 18, color: 'grey' }]}
//         />
//         {/* <Label>{props.placeholder}</Label> */}
//         {/* <Image
//           resizeMode={"contain"}
//           style={props.style}
//           source={props.icons}
//         /> */}
//              <Picker
//             selectedValue={this.state.eventlocation}
//             style={{ height: 40, color: "#656D68" }}
//             onValueChange={(itemValue, itemIndex) => this.setState({eventlocation:itemValue})}>
//             <Picker.Item label="Pick your location" value="4" />
//             <Picker.Item label="Lahore" value="1" />
//             <Picker.Item label="Islamabad" value="2" />
//             <Picker.Item label="Karachi" value="3" />

//           </Picker>

       
//       </Item>
//     </View>
//                  <InputText
//                   icon="person-outline"
//                   placeholder="Number of tickets "
//                   autoCapitalize="none"
//                   value={this.state.tickets}
//                   keyboardType={'phone-pad'}
//                   onChangeText={name => this.setState({ tickets:name })}
//                 />
//                 <InputText
//                   icon="call"
//                   placeholder="Phone Number"
//                   autoCapitalize="none"
//                   value={this.state.phone}
//                   onChangeText={phone => this.setState({ phone })}
//                   keyboardType={'phone-pad'}
                
//                 />

//                 <View style={{borderBottomWidth:0.8,borderBottomColor:"gray",marginLeft:30,marginRight:20,marginTop:5}}>
//                   <View style={{flexDirection:"row",alignItems:"center"}}>
//                 <Icon
//                     type="MaterialIcons"
//                     name="insert-invitation"
//                     style={[{ fontSize: 18, top: 11,marginLeft:5, color: 'grey' }]}
//                   />
//                   <DatePicker
//                   defaultDate={new Date(2019, 9, 6)}
//                   minimumDate={new Date(2019, 1, 1)}
//                   maximumDate={new Date(2019, 12, 31)}
//                   locale={"en"}
//                   timeZoneOffsetInMinutes={undefined}
//                   modalTransparent={false}
//                   animationType={"fade"}
//                   androidMode={"default"}
//                   placeHolderText="Date"
//                   textStyle={{ color: "grey",top:10,left:2}}
//                   placeHolderTextStyle={{ color: "grey",top:10,left:2}}
//                   onDateChange={this.setDate}
//                   disabled={false}
//                 />
  
//                 </View>
//                 </View>
//               </Form>

//               <View style={{ marginTop: 60 }}>
//                 <TouchableOpacity onPress={this.onSubmit}>
//                   <View
//                     style={[
//                       {
//                         backgroundColor: "#df1d25",
//                         width: width * 0.7,
//                         alignItems: "center",
//                         width: 300,
//                         paddingVertical: 8,
//                         marginBottom: 5,
//                         alignSelf: "center"
//                       }
//                     ]}
//                   >
//                     <Text
//                       style={[
//                         {
//                           fontSize: 18,
//                           textAlign: "center",
//                           color: "white",
//                           fontWeight: "bold"
//                         }
//                       ]}
//                     >
//                       Buy
//                     </Text>
//                   </View>
//                 </TouchableOpacity>
//               </View>
              
//             </View>
//         </KeyboardAvoidingView>
//       </ImageBackground>
//       </ScrollView>

//     );
//   }
// }

// export default BuyTicket;

import React, { Component } from 'react';
import { StyleSheet, ActivityIndicator, View,TouchableHighlight,Text,TouchableOpacity } from 'react-native';
import { WebView } from "react-native-webview";
import { WP } from '../responsive';
import {

  Icon,

} from "native-base";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
export default class MainActivity extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: true };
  }

  showSpinner() {
    console.log('Show Spinner');
    this.setState({ visible: true });
  }

  hideSpinner() {
    console.log('Hide Spinner');
    this.setState({ visible: false });
  }

  render() {
    return (
      <View
        style={this.state.visible === true ? styles.stylOld : styles.styleNew}>
        {this.state.visible ? (
          <ActivityIndicator
            color="red"
            size="large"
            style={styles.ActivityIndicatorStyle}
          />
        ) : null}

        <WebView
          style={styles.WebViewStyle}
          //Loading URL
          source={{ uri:'https://www.easytickets.pk/detail/index/6LCTg' }}
          //Enable Javascript support
          //For the Cache
          //View to show while loading the webpage
          //Want to show the view or not
          //startInLoadingState={true}
          onLoadStart={() => this.showSpinner()}
          onLoad={() => this.hideSpinner()}
        />
         <TouchableOpacity style={{position:'absolute',bottom:WP('10'),left:WP('5'),zIndex:1,height:WP('15'),width:WP('15'),backgroundColor:'red',borderRadius:100,alignItems:'center',justifyContent:'center'}}
         onPress = {()=>this.props.navigation.goBack()}
         >
                               
                   <Icon
    type="MaterialIcons"
    name="arrow-back"
    style={[
      { color: "black", fontSize: 28, }
    ]}
  />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  stylOld: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  styleNew: {
    flex: 1,
  },
  WebViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginTop: 40,
  },
  ActivityIndicatorStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
});
