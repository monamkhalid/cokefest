import React, { Component } from 'react';
import { Image,View ,ScrollView,ImageBackground} from 'react-native';
import {Header, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import Swiper1 from '../assets/images/Swiper1.jpg';

 class SocialFeed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [{
        name: 'Sankhadeep',
        post: 'posted',
        time:'2h',
        text:"NativeBase is made from effective",
        paragraph:'NativeBase is made from effective building blocks referred to as components.'

    },
    {
      name: 'Sankhadeep',
      post: 'posted',
      time:'2h',
      text:"NativeBase is made from effective",
      paragraph:'NativeBase is made from effective building blocks referred to as components.'

  },
  {
    name: 'Sankhadeep',
    post: 'posted',
    time:'2h',
    text:"NativeBase is made from effective",
    paragraph:'NativeBase is made from effective building blocks referred to as components.'

},
{
  name: 'Sankhadeep',
  post: 'posted',
  time:'2h',
  text:"NativeBase is made from effective",
  paragraph:'NativeBase is made from effective building blocks referred to as components.'

},

  ]
       
    };
}

  render() {
    return (
        <View style={{flex:1,marginBottom:10}}>
        <Header
              hasSegment
              style={[
                { backgroundColor: "#E52526",marginTop:22 },
              ]}
            >
              <Left>
                <Button 
                  transparent
                  onPress={() => {
                    this.props.navigation.openDrawer()
                    }}
                >
                  <Icon
                    type="MaterialIcons"
                    name="dehaze"
                    style={[ { color: "white" }]}
                  />
                </Button>
              </Left>
              <Body>
                <Text
                  style={[
                    { color: "white",alignSelf:"center",fontSize:18 },
                   
                    
                  ]}
                >
                  Social Feed
                </Text>
              </Body>
              <Right>
             
              </Right>
            </Header>
      <ScrollView>
      {this.state.list.map((l, i) => (
          <Card key={i}>
            <CardItem>
              <Left>
                <Thumbnail source={Swiper1} />
                <Body>
                  <Text style={{fontSize:16,color:"gray",width:200}} >{l.name}</Text>
                  <Text style={{fontSize:14,color:"gray"}}>{l.post}</Text>
                </Body>
               
              </Left>
              <Right>
                <View style={{flexDirection:"row"}}>
              <Icon  type="MaterialIcons"
                    name="alarm" style={{color:"gray"}} />
                <Text style={{marginLeft:5,color:"gray"}}>{l.time}</Text></View></Right>
                
            </CardItem>
            <CardItem cardBody>
            <ImageBackground style={{ flex: 1 ,height: 200, width: null,justifyContent:"flex-end",}} source={Swiper1}>
              <Text style={{color:'white',marginLeft:15,marginBottom:15,fontSize:14}}>{l.text}</Text>
              </ImageBackground>
            
            </CardItem>
            <CardItem>
              <Text style={{color:"gray",fontSize:14}}>{l.paragraph}</Text>
            </CardItem>
          </Card>
      ))}
          </ScrollView>
      </View>
    );
  }
}
export default SocialFeed;