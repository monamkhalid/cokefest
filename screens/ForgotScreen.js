import React from "react";
import { Form } from "native-base";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView,
  ScrollView,ActivityIndicator,
  Alert
} from "react-native";
import { Header } from "react-navigation";
import { apiUrl } from "../config/api";
import { StackActions, NavigationActions } from 'react-navigation';

import InputText from "../components/InputText";
import background from '../assets/images/artWork.jpeg';
import axios from "axios";

class Forgot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      showLumper:false
    };
  }

  recoverPassword = () => {
    let { email } = this.state;
    state = this.state;
    const { navigate } = this.props.navigation;
    if (email.length < 7) {
      Alert.alert("Alert", "Please enter a valid phone number.");
    } else {
      this.setState({showLumper:true})
      axios
        .get(apiUrl + "/forget_password", {
          params: {
            keyword: email
          }
        })
        .then((response)=> {
          this.setState({showLumper:false})

          console.log(">>>",response.data.code );
          if (response.data.code) {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'VerificationScreen' })],
            });
            this.props.navigation.dispatch(resetAction);
          } else {
            Alert.alert("Alert", "Phone number not found, try again.");
          }
        })
        .catch((error) =>{
          console.log("show eror",error)
          this.setState({showLumper:false})

          Alert.alert("Alert", "Bad request error.");
        });
    }
  };
  //   this.props.navigation.navigate("RetypeScreen");
  render() {
    const { seg } = this.state;
    var { height, width } = Dimensions.get("window");

    return (
      <ImageBackground style={{ flex: 1 }} source={background}>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          keyboardVerticalOffset={Header.HEIGHT + 90}
          behavior="padding"
        >
          <ScrollView>
            <View style={{ height: height * 0.3 }} />
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  color: "grey",
                  fontSize: 20,
                  marginLeft: 30,
                  fontWeight: "bold"
                }}
              >
                Forgot Password?
              </Text>
              <Form style={{ marginTop: 30 }}>
                <InputText
                  icon="mail-outline"
                  placeholder="03xxxxxxxxx"
                  onChangeText={text => this.setState({ email: text })}
                  value={this.state.email}
                  keyboardType={'phone-pad'}
                />
              </Form>

              <View style={{ marginTop: 60 }}>
                {/* <TouchableOpacity onPress={this.recoverPassword}>
                  <View
                    style={[
                      {
                        backgroundColor: "#df1d25",
                        width: width * 0.7,
                        alignItems: "center",
                        width: 300,
                        paddingVertical: 8,
                        marginBottom: 5,
                        alignSelf: "center"
                      }
                    ]}
                  >
                    <Text
                      style={[
                        {
                          fontSize: 18,
                          textAlign: "center",
                          color: "white",
                          fontWeight: "bold"
                        }
                      ]}
                    >
                      Submit
                    </Text>
                  </View>
                </TouchableOpacity> */}
                   {this.state.showLumper?
              <View >
              <View
                style={[
                  {
                    backgroundColor: "#df1d25",
                    width: width * 0.7,
                    alignItems: "center",
                    width: 300,
                    paddingVertical: 8,
                    marginBottom: 5,
                    alignSelf: "center"
                  }
                ]}
              >
                       <ActivityIndicator size="small" color="#fff" />

              </View>
            </View>
          :
          <TouchableOpacity  onPress={this.recoverPassword}>
          <View
            style={[
              {
                backgroundColor: "#df1d25",
                width: width * 0.7,
                alignItems: "center",
                width: 300,
                paddingVertical: 8,
                marginBottom: 5,
                alignSelf: "center"
              }
            ]}
          >
            <Text
              style={[
                {
                  fontSize: 18,
                  textAlign: "center",
                  color: "white",
                  fontWeight: "bold"
                }
              ]}
            >
             Submit
            </Text>
          </View>
        </TouchableOpacity>

          }
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

export default Forgot;
