import React from "react";
import { Header, Left, Body, Right,Button,Icon} from "native-base";
import { View, Text, Dimensions, ImageBackground, Image, Alert, FlatList } from "react-native";
import background from '../assets/images/artWork.jpeg';
import { apiUrl } from "../config/api";
import _isEmpty from 'lodash/isEmpty';
import { WP } from "../responsive";

class Findfriends extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  componentDidMount(){
    // axios
    //     .get(apiUrl + "/getfoodstall")
    //     .then(function(response) {
    //       this.setState({data: response.data})
    //     })
    //     .catch(function(error) {
    //       Alert.alert("Alert", JSON.stringify(error));
    //     });
      
        fetch(apiUrl + '/getfoodstall')
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({data: responseJson})
        })
        .catch((error) => {
          Alert.alert("Alert", JSON.stringify(error));
        });
  }

  renderFlatView = ({item}) => {
    // console.log('==item==', item.image)
    var { height, width } = Dimensions.get("window");

    return(
      <View style={{flexDirection:'row'}}>
      <Image
            // source={require("../assets/images/Buy-your-ticket1.png")}
            source={{ uri: item.image }}
            style={{
              height: height * 0.1,
              width: width * 0.25,
            }}
          />
      </View>
      
    );
  }

  render() {
    var { height, width } = Dimensions.get("window");
    const {data} = this.state;
    if(_isEmpty(data)){
    }
    return (
      <ImageBackground style={{ flex: 1 }} source={background}>
        <Header
          hasSegment
          style={[{ backgroundColor: "#E52526", marginTop: 22,height:WP('20') }]}
        >
          <Left>
          <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialIcons"
                name="arrow-back"
                style={[{ color: "white" }]}
              />
            </Button>
          </Left>

          <Body>
            <Text
              style={[{ color: "white", fontSize: 18 }]}
            >
              Food Stalls
            </Text>
          </Body>
          <Right />
        </Header>

        <FlatList
          contentContainerStyle={{flexGrow: 1,alignItems:'center'}}
          data={data}
          renderItem={(category) => (
            <View style = {{display:"flex",height:110,width:110,margin:5,borderWidth:1,borderColor:"#000"}}>
            <Image
             source={{ uri: category.item.image }}
             style = {{height:"100%",width:"100%"}}
             resizeMode = "contain"
            
            />

            </View>
          )}
          keyExtractor={item => item.foodstall_id}
          numColumns={3}
        />

        {/* <View style={{ marginTop: 50 }}>
          <Text style={{ color: "gray", marginLeft: 20 }}>Regular</Text>
        </View>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: "gray",
            marginRight: 20,
            marginLeft: 20
          }}
        />
        <View style={{ flexDirection: "row" }}>
          <Image
            source={require("../assets/images/Buy-your-ticket1.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          />

          <Image
            source={require("../assets/images/Book-your-stall.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          />

          <Image
            source={require("../assets/images/find-my-friend.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          />
        </View>
        <View style={{ marginTop: 20 }}>
          <Text style={{ color: "gray", marginLeft: 20 }}>Premium</Text>
        </View>
        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: "gray",
            marginRight: 20,
            marginLeft: 20
          }}
        />
        <View style={{ flexDirection: "row" }}>
          <Image
            source={require("../assets/images/Buy-your-ticket1.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          />

          <Image
            source={require("../assets/images/Book-your-stall.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          />

          <Image
            source={require("../assets/images/find-my-friend.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          />
        </View>
        <View style={{ marginTop: 20 }}>
          <Text style={{ color: "gray", marginLeft: 20 }}>Corporate</Text>

          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: "gray",
              marginRight: 20,
              marginLeft: 20
            }}
          />
          <View style={{ flexDirection: "row" }}>
            <Image
              source={require("../assets/images/Buy-your-ticket1.png")}
              style={{
                height: height * 0.1,
                width: width * 0.25,
                resizeMode: "contain"
              }}
            />

            <Image
              source={require("../assets/images/Book-your-stall.png")}
              style={{
                height: height * 0.1,
                width: width * 0.25,
                resizeMode: "contain"
              }}
            />

            <Image
              source={require("../assets/images/find-my-friend.png")}
              style={{
                height: height * 0.1,
                width: width * 0.25,
                resizeMode: "contain"
              }}
            />
          </View>
        </View>
        <View style={{ marginTop: 20 }}>
          <Text style={{ color: "gray", marginLeft: 20 }}>
            Home Based Resturant
          </Text>

          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: "gray",
              marginRight: 20,
              marginLeft: 20
            }}
          />
          <View style={{ flexDirection: "row" }}>
            <Image
              source={require("../assets/images/Buy-your-ticket1.png")}
              style={{
                height: height * 0.1,
                width: width * 0.25,
                resizeMode: "contain"
              }}
            />

            <Image
              source={require("../assets/images/Book-your-stall.png")}
              style={{
                height: height * 0.1,
                width: width * 0.25,
                resizeMode: "contain"
              }}
            />

            <Image
              source={require("../assets/images/find-my-friend.png")}
              style={{
                height: height * 0.1,
                width: width * 0.25,
                resizeMode: "contain"
              }}
            />
          </View>
        </View> */}
      </ImageBackground>
    );
  }
}

export default Findfriends;
