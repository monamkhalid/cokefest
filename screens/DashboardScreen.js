import React from "react";
import { commonStyles } from "./styles";
import {
  Segment,
  Button,
  Form,
  Header,
  Item,
  Input,
  Icon,
  Left,
  Card,
  Content,
  Right,
  Body
} from "native-base";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Alert,
  StatusBar,
  Image,
  ImageBackground,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,BackHandler,AsyncStorage,ActivityIndicator
} from "react-native";
import InputText from "../components/InputText";
import Swiper from "react-native-swiper";
import { withNavigationFocus } from 'react-navigation';
import music from "../assets/images/Music-Lineup-draw.png";
import navigation from "../assets/images/navigation.png";
import ticket from "../assets/images/Buy-your-ticket1.png";
import food from "../assets/images/foodstallgood.png";
import { Video } from 'expo-av';
import { apiUrl } from "../config/api";
import axios from "axios";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {WP,HP} from '../responsive';
import VideoPlayer from 'expo-video-player'
import WebView from 'react-native-webview'
class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      seg: 2,
      slider: [],
      videos: [],
      displayName:"",
      customeMode:"stretch",
      images:[{
        "homepost_id": "2",
        "image": "http://soulfest.pk/cdn/homepost/2.jpg",
        "title": "SoulFest - Lahore",
        "url": "http://activemedia.com.pk/",
      }]
    };
    this.getKey()
  }
  async getKey() {
    try {
      const value = await AsyncStorage.getItem('@MySuperStore:key');
      if(value){
          let newProduct = JSON.parse(value);
          this.setState({
            displayName: newProduct.name,
           

          })



      }
      else {

     
      }

    } catch (error) {
      console.log("Error retrieving data" + error);
    }
  }

  componentWillMount() {
  
    let state = this;
    axios
      .get(apiUrl + "/getslider")
      .then(function(response) {
        state.setState({ slider: response.data });
      })
      .catch(function(error) {
        Alert.alert("Alert", "Bad request error.");
      });
    axios
      .get(apiUrl + "/getvideos")
      .then(function(response) {
        state.setState({ videos: response.data });
      })
      .catch(function(error) {
        Alert.alert("Alert", "Bad request error.");
      });
      axios
      .get(apiUrl + "/gethomepost")
      .then(function(response) {
     
        state.setState({ images: response.data });
      })
      .catch(function(error) {
        Alert.alert("Alert", "Bad request error.");
      });
  }

  videoController = async(id) => {
    const { videos } = this.state;
    for (let i = 0; i < videos.length; i++) {
        if ( id === videos[i].video_id ) {
          videos[i].isPlaying = true;

        } else {
          videos[i].isPlaying = false;
        }      
    }
    this.setState({ videos: videos })
  }
  // _onPlaybackStatusUpdate = (playbackStatus,id) => {
  //   console.log("Showing id",id)

  //   console.log("Showing status",playbackStatus)
  //   if (playbackStatus.isPlaying)
  //   {
  //     this.videoController(id)
  //   }
  //     // The player has just finished playing and will stop.
  // };
  
 update = (player)=>{
   if(player.fullscreenUpdate == 1){
     this.setState({customeMode:"contain"})
   }
   else  if(player.fullscreenUpdate == 3){
    this.setState({customeMode:"stretch"})

  }
  
 }
 renderImage = ()=>{
   this.state.images.map((images)=>{
    return(
      <TouchableOpacity
      style = {{display:'flex',width: "100%", height: WP('60')}}
      >
        <Image
        source = {{uri:images.image}}
        resizeMode = "contain"
        style = {{height:"100%",width:"100%"}}
        
        />
      
      </TouchableOpacity>
     )

   })
   
  
 }
  render() {
    const { seg, slider, videos ,images} = this.state;
    var { height, width } = Dimensions.get("window");
    console.log("showing videos",images)

    return (
      <KeyboardAwareScrollView>

      <View style={{ flex: 1, backgroundColor: "white" }}>
        {/* <Header
          hasSegment
          style={[
            { backgroundColor: "white", marginTop: 25 },

            commonStyles.borderBottomWidth1,
            commonStyles.shadowbox
          ]}
        >
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.openDrawer();
              }}
            >
              <Icon 
                type="MaterialIcons"
                name="dehaze"
                style={[commonStyles.fontSize18, { color: "black" }]}
              />
            </Button>
          </Left>
          <Body></Body>
          <Right
          style = {{display:"flex",justifyContent:'center',alignItems:'center',}}
          >
            <Text style = {{color:"#000",fontWeight:'bold'}}>Hi {this.state.displayName}</Text>
            <Button transparent  onPress={() => {
                this.props.navigation.navigate("EditProfile");
              }}>
              <Icon
                type="MaterialIcons"
                name="person-outline"
                style={[commonStyles.fontSize18, { color: "black" }]}
              />
            </Button>
          </Right>
        </Header> */}
              <View style = {{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginTop:50}}>
               <Button style={{}}
                            transparent
                            onPress={() => {
                                this.props.navigation.openDrawer()
                                }}
                        >
                            <Icon
                                type="MaterialIcons"
                                name="dehaze"
                                style={[commonStyles.fontSize18, { color: "black" }]}
                            />
                        </Button>
                        <View style = {{display:'flex',flexDirection:'row',alignItems:'center'}}>
                        <Text style = {{color:"#000",fontWeight:'bold'}}>Hi {this.state.displayName}</Text>
                        <Button transparent  onPress={() => {
                this.props.navigation.navigate("EditProfile");
              }}>
              <Icon
                type="MaterialIcons"
                name="person-outline"
                style={[commonStyles.fontSize18, { color: "black" }]}
              />
            </Button>

                        </View>
              

                  

               </View>
                      
        <View
          style={{
            minHeight: 160,
            backgroundColor: "white"
          }}
        >
          <Swiper
             key={slider.length}

            showsButtons={true}
            loop={true}
            autoplayTimeout={5}
            autoplay = {true}
            autoplayDirection = {true}
            dotColor="#E3E3E3"
            activeDotColor="grey"
            nextButton={<Text style={{ color: "white", fontSize: 40 }}>›</Text>}
            prevButton={<Text style={{ color: "white", fontSize: 40 }}>‹</Text>}
            paginationStyle={{ bottom: 10 }}
            paginationStyleItem={{
              width: 5,
              height: 5,
              marginLeft: 5,
              marginRight: 5
            }}
            style={{
              display:'flex',
              height:WP('35'),
              minHeight: 160
            }}
          >
            {slider.map((l, i) => (
              <View key={i}> 
                <Image
                  key={i}
                  resizeMode={"cover"}
                  style={[{ height: "100%", width: "100%" }]}
                  source={{ uri: l.image }}
                />
              </View>
            ))}
          </Swiper>
        </View>
        <View
          style={{ flexDirection: "row", alignSelf: "center", marginTop: 20 }}
        >
          <TouchableOpacity
          onPress = {()=>{this.props.navigation.navigate('BuyTicket')}}
          >
            <Image
              resizeMode={"contain"}
              style={[
                {
                  height: height * 0.12,
                  width: width * 0.25
                }
              ]}
              source={ticket}
            />
          </TouchableOpacity>
          <TouchableOpacity
          onPress = {()=>{this.props.navigation.navigate('Navigation')}}
          >
            <Image
              resizeMode={"contain"}
              style={[
                {
                  height: height * 0.12,
                  width: width * 0.25
                }
              ]}
              source={require('../assets/images/navigation.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
          onPress = {()=>{this.props.navigation.navigate('FoodStall')}}
          >
            <Image
              resizeMode={"contain"}
              style={[
                {
                  height: height * 0.12,
                  width: width * 0.25
                }
              ]}
              source={food}
            />
          </TouchableOpacity>
          <TouchableOpacity
          onPress = {()=>{this.props.navigation.navigate('LineupScreen')}}
          >
            <Image
              resizeMode={"contain"}
              style={[
                {
                  height: height * 0.12,
                  width: width * 0.25
                }
              ]}
              source={music}
            />
          </TouchableOpacity>
        </View>
        {/* <View style={{ marginTop: 20, marginHorizontal: 10 }}>
          <Item
            style={{ backgroundColor: "#dee0df", borderRadius: 20, height: 30 }}
          >
            <Input
              placeholder="SEARCH"
              style={{ marginLeft: 10, fontSize: 14 }}
            />
            <Icon active name="search" style={{ marginRight: 10 }} />
          </Item>
        </View> */}
        <ScrollView contentContainerStyle = {{flexGrow:1}}>
          {videos.length > 0
          ?
          <View style = {{display:'flex'}}>

        <View style = {{display:'flex'}}
        key = {Math.random()}
        >
                 <View style={{ flexDirection: "row",alignItems:'center' }}>
             <Icon
               type="MaterialIcons"
               name="play-arrow"
               style={[{ color: "grey" }]}
             />
             <Text
               style={{
                 fontWeight: "bold",
                 fontSize: 14,
                 color: "grey"
               }}
             >
               {videos[0].title}
             </Text>
           </View>
           </View>
           {/* <Video
          source={{ uri:videos[0].video }}
          rate={1.0}
          volume={1.0}
          isMuted={false}
          resizeMode='contain'
          useNativeControls = {true}
          style={{ width: "100%", height: WP('60') }}
        /> */}
          <WebView
          style={{ width: "100%", height: WP('60') }}
          //Loading URL
          source={{ uri:this.props.isFocused ? videos[0].video :'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQArGhBAQ2gSv7B02J5RCpny7Z03G30R460U5aWqZifRiZBPui9&s'}}
          //Enable Javascript support
          javaScriptEnabled={true}
          //For the Cache
          domStorageEnabled={true}
          allowsFullscreenVideo = {true}
          autoplay = {true}
          paused = {true}
          userAgent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"


          
          //View to show while loading the webpage
          //Want to show the view or not
          //startInLoadingState={true}
       
        />

           </View>
        :
        <ActivityIndicator size = "large" color = "red" style = {{height: WP('60')}} />
        

          }
           {images.map((l, i) => (
             <View style = {{display:'flex'}}
             key = {i}
             >
                      <View style={{ flexDirection: "row",alignItems:'center' }}>
                  <Icon
                    type="MaterialIcons"
                    name="play-arrow"
                    style={[{ color: "grey" }]}
                  />
                  <Text
                    style={{
                      fontWeight: "bold",
                      fontSize: 14,
                      color: "grey"
                    }}
                  >
                    {l.title}
                  </Text>
                </View>
                    <TouchableOpacity key = {i}
              style = {{display:'flex',width: "100%", height: WP('60')}}
              onPress = {()=>{this.props.navigation.navigate("FullScreenVideo",{params:l.url})}}
              >
             
                <Image
                source = {{uri:l.image}}
                resizeMode = "contain"
                style = {{height:"100%",width:"100%"}}
                
                />
              
              </TouchableOpacity>
                
             </View>

             
          
          ))}
   


        </ScrollView>
      </View>
      </KeyboardAwareScrollView>

    );
  }
}

export default withNavigationFocus(Dashboard);

const styles = StyleSheet.create({
  wrapper: {},
  slide1: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#9DD6EB"
  },
  slide2: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#97CAE5"
  },
  slide3: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#92BBD9"
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  }
});
