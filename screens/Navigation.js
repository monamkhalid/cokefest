import React, { Component } from 'react';
import { Text, View, StyleSheet,ImageBackground,TouchableOpacity,Linking,Image,ScrollView } from 'react-native';

import {
  Segment,
  Button,
  Form,
  Header,
  Item,
  Input,
  Icon,
  Left,
  Card,
  Content,
  Right,
  Body
} from "native-base";

import axios from "axios";
import { apiUrl } from "../config/api";
import { WP } from '../responsive';
import Modal from "react-native-modal";
import background from '../assets/images/artWork.jpeg';

export default class Navigationlist extends Component {
  constructor(props){
    super(props);
    this.state = {
      images:"",
      Lahore:"",
      Karachi:"",
      islamabad:"",
    }
  }
  openMaps =applocationid =>{
    axios
    .get(apiUrl + "/getlocation")
    .then((response)=> {
      console.log("show links on maps",response)
      if(response.data){
        let obj = response.data.find(obj => obj.location_id == applocationid);
        console.log("filtered obj",obj)
        if(obj){
          if(obj.path){
                Linking.openURL(obj.path).catch((err) => console.error('An error occurred', err));


          }

        }


      }
    })
    .catch((error)=> {
      console.log("show links on maps",error)

    });
  }
  componentDidMount(){
    let state = this;
    axios
      .get(apiUrl + "/getmapimage")
      .then((response)=> {
        // state.setState({ images: response.data });
        console.log("response of naviagtion",response)
        let karachiimage = response.data.find(obj => obj.city == "Karachi");
        let lahoreimage = response.data.find(obj => obj.city == "Lahore");
        let islamabadimage = response.data.find(obj => obj.city == "Islamabad");
        console.log("data filtered",karachiimage)
        if(karachiimage)
        {
          this.setState({Karachi:karachiimage})
        }
        if(lahoreimage)
        {
          this.setState({Lahore:lahoreimage})
        }
        if(islamabadimage)
        {
          this.setState({islamabad:islamabadimage})
        }



      })
      .catch(function(error) {
        Alert.alert("Alert", "Bad request error.");
      });
  }


  render() {
    return (
      <ScrollView contentContainerStyle={{ flexGrow: 1,backgroundColor:"#ffff" }} >
      <ImageBackground style={{ flex: 1 }} source={background}>

      {/* <Text>Lahore</Text> */}
      {/* <View
          style={{ marginTop: 50, flexDirection: "row", alignItems: "center", }}
        >
          <Button
            transparent
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={[{ color: "black", fontSize: 30, marginLeft: 20 }]}
            />
          </Button>
          <Text style={{ marginLeft: 10, fontSize: 20 }}>Navigation</Text>
        </View> */}
           <Header
          hasSegment
          style={[{ backgroundColor: "#E52526", marginTop: 22,height:WP('20')  }]}
        >
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialIcons"
                name="arrow-back"
                style={[{ color: "white" }]}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={[{ color: "white", alignSelf: "flex-start", fontSize: 18 }]}
            >
            Navigation
            </Text>
          </Body>
          <Right></Right>
        </Header>
        {/* <View style = {{display:'flex',height:WP('45'),width:"100%",alignItems:'center',justifyContent:"flex-end"}}>
          <Image
        source={require("./../assets/images/logo.png")}
        style={{ width: WP('45'), height: WP('45'), resizeMode: "contain",marginTop:WP('3') }}
      />

          </View>
        <View style = {{display:'flex',alignItems:"center",justifyContent:'center',marginTop:WP('40')}}>
        <View style = {{display:'flex',alignItems:"center",justifyContent:'center',height:WP('10')}}>
        <View style = {{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'flex-start'}}>

<TouchableOpacity
onPress = {()=>{this.openMaps(1)}}
style = {{display:'flex',flexDirection:'row',justifyContent:'center',alignItems:'center',}}

>

<Text style = {{color:"grey",fontWeight:'bold',fontSize:WP('4')}}>Lahore  </Text>



</TouchableOpacity>
{this.state.Lahore.image 
?
<TouchableOpacity
// style = {{marginLeft:WP('5')}}
style = {{height:WP('30'),width:WP('30'),alignItems:'center',justifyContent:'center',marginTop:WP('1'),marginLeft:WP('20')}}
onPress= {()=>this.props.navigation.navigate('ImageDetails',{images:this.state.Lahore})}

>
<Image
source = {require('./../assets/images/Lahore.png')}
style = {{height:"100%",width:"100%",justifyContent:'center'}}
resizeMode = {"contain"}

/>
</TouchableOpacity>
:
null

}
</View>
        <View style = {{display:'flex',flexDirection:'row',alignItems:'center',padding:WP('5')}}>

        <TouchableOpacity
        onPress = {()=>{this.openMaps(3)}}
        style = {{display:'flex',flexDirection:'row',justifyContent:'center',alignItems:'center'}}

        >
     <Text style = {{color:"grey",fontWeight:'bold',fontSize:WP('4'),}}>Karachi </Text>
    

        </TouchableOpacity>
        {this.state.Karachi.image
        ?
        <TouchableOpacity
        style = {{height:WP('30'),width:WP('30'),alignItems:'center',justifyContent:'center',marginTop:WP('1'),marginLeft:WP('20')}}

        onPress= {()=>this.props.navigation.navigate('ImageDetails',{images:this.state.Karachi})}

        >
          <Image
source = {require('./../assets/images/Karachi.png')}
style = {{height:"100%",width:"100%",justifyContent:'center'}}
          resizeMode = {"contain"}
          
          />
          </TouchableOpacity>
          :
          null
        
        }
      
        </View>
  
        <View style = {{display:'flex',flexDirection:'row',alignItems:'center',}}>
        <TouchableOpacity
        onPress = {()=>{this.openMaps(2)}}
        style = {{display:'flex',flexDirection:'row',justifyContent:'space-around',alignItems:'center'}}

        >

<Text style = {{color:"grey",fontWeight:'bold',fontSize:WP('4'),}}>Islamabad</Text>
       
      
        </TouchableOpacity>
         {
          this.state.islamabad.image?
          <TouchableOpacity
          onPress= {()=>this.props.navigation.navigate('ImageDetails',{images:this.state.islamabad})}
          style = {{height:WP('30'),width:WP('30'),alignItems:'center',justifyContent:'center',marginTop:WP('1'),marginLeft:WP('18')}}
          >
          <Image
source = {require('./../assets/images/Islamabad.png')}
style = {{height:"100%",width:"100%",justifyContent:'center'}}
          resizeMode = {"contain"}
          
          />
          </TouchableOpacity>
          :
          null

        }
        
        </View>



        </View>
        </View>
    */}
    {this.state.Lahore 
    ?
    <View style = {{display:'flex',flexDirection:'row',alignItems:"center",justifyContent:'center',marginTop:40}}>
   
   
    <ImageBackground 
    source = {require('../assets/images/box1.png')}
    style = {{display:'flex',height:WP('35'),width:WP('30'),marginRight:WP(-13),zIndex:1,alignItems:'center',justifyContent:'center'}}
    >
    <TouchableOpacity
    onPress = {()=>{this.openMaps(1)}}
    style = {{alignItems:'center',justifyContent:'center'}}

    >
    <Image
    source = {require('../assets/images/pin.png')}
    style = {{height:WP('10'),width:WP('10'),resizeMode:"contain",alignSelf:"center"}
  
  }
    />
    
    <Text style = {{fontWeight:'bold',fontSize:WP('2')}}>Click for Location</Text>

    <Text style = {{fontWeight:'bold',fontSize:WP('4')}}>{this.state.Lahore.city}</Text>
    </TouchableOpacity>

    </ImageBackground>
    <ImageBackground 
        source = {require('../assets/images/box2.png')}

    style = {{display:'flex',height:WP('40'),width:WP('40'),alignItems:'center',justifyContent:'center'}}>
    <TouchableOpacity
              onPress= {()=>this.props.navigation.navigate('ImageDetails',{images:this.state.Lahore})}

    >
    <Image
      source = {{uri:this.state.Lahore.image}}
      style = {{height:WP('20'),width:WP('20')}}
      resizeMode = {'contain'}
      />
    </TouchableOpacity>

    </ImageBackground>

    </View>
    :
    null

    
    }
       {this.state.Karachi 
    ?
    <View style = {{display:'flex',flexDirection:'row',alignItems:"center",justifyContent:'center',marginTop:40}}>
   
   
    <ImageBackground 
    source = {require('../assets/images/box1.png')}
    style = {{display:'flex',height:WP('35'),width:WP('30'),marginRight:WP(-13),zIndex:1,alignItems:'center',justifyContent:'center'}}
    >
    <TouchableOpacity
    onPress = {()=>{this.openMaps(3)}}
    style = {{alignItems:'center',justifyContent:'center'}}


    >
     <Image
    source = {require('../assets/images/pin.png')}
    style = {{height:WP('10'),width:WP('10'),resizeMode:"contain",alignSelf:"center"}
  
  }
    />
        <Text style = {{fontWeight:'bold',fontSize:WP('2')}}>Click for Location</Text>

    <Text style = {{fontWeight:'bold',fontSize:WP('4')}}>{this.state.Karachi.city}</Text>
    </TouchableOpacity>

    </ImageBackground>
    <ImageBackground 
        source = {require('../assets/images/box2.png')}

    style = {{display:'flex',height:WP('45'),width:WP('40'),alignItems:'center',justifyContent:'center'}}>
    <TouchableOpacity
    
    onPress= {()=>this.props.navigation.navigate('ImageDetails',{images:this.state.Karachi})}
    >
    <Image
      source = {{uri:this.state.Karachi.image}}
      style = {{height:WP('20'),width:WP('20')}}
      resizeMode = {'contain'}
      />
          </TouchableOpacity>

    </ImageBackground>

    </View>
    :
    null

    
    }
       {this.state.islamabad 
    ?
    <View style = {{display:'flex',flexDirection:'row',alignItems:"center",justifyContent:'center',marginTop:40}}>
   
   
    <ImageBackground 
    source = {require('../assets/images/box1.png')}
    style = {{display:'flex',height:WP('35'),width:WP('30'),marginRight:WP(-13),zIndex:1,alignItems:'center',justifyContent:'center'}}
    >
    <TouchableOpacity
      onPress = {()=>{this.openMaps(2)}}
      style = {{alignItems:'center',justifyContent:'center'}}


    >
     <Image
    source = {require('../assets/images/pin.png')}
    style = {{height:WP('10'),width:WP('10'),resizeMode:"contain",alignSelf:"center"}
  
  }
    />
        <Text style = {{fontWeight:'bold',fontSize:WP('2')}}>Click for Location</Text>

    <Text style = {{fontWeight:'bold',fontSize:WP('4')}}>{this.state.islamabad.city}</Text>
    </TouchableOpacity>

    </ImageBackground>
    <ImageBackground 
        source = {require('../assets/images/box2.png')}

    style = {{display:'flex',height:WP('45'),width:WP('40'),alignItems:'center',justifyContent:'center'}}>
    <TouchableOpacity
              onPress= {()=>this.props.navigation.navigate('ImageDetails',{images:this.state.islamabad})}

    >
      <Image
      source = {{uri:this.state.islamabad.image}}
      style = {{height:WP('20'),width:WP('20')}}
      resizeMode = {'contain'}
      />
    </TouchableOpacity>

    </ImageBackground>

    </View>
    :
    null

    
    }
  
  
    
      </ImageBackground>
      </ScrollView> 
    );
  }
}