import React from "react";
import { Header, Left, Body, Right, Button, Icon } from "native-base";
import {
  View,
  Text,
  Dimensions,
  ImageBackground,
  Image,
  TouchableOpacity,
  Linking,
  Picker,
  ScrollView,
  Platform
} from "react-native";
import background from "../assets/images/SignIn.png";
import Swiper1 from "../assets/images/Swiper1.jpg";
import { apiUrl } from "../config/api";
import axios from "axios";
import {WP,HP} from '../responsive';
import RNPickerSelect from 'react-native-picker-select';

class HelpScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: []
    };
  }
  componentWillMount() {
    let state = this;
    axios
      .get(apiUrl + "/getatristlineup")
      .then(function(response) {
        state.setState({ list: response.data });
      })
      .catch(function(error) {
        Alert.alert("Alert", "Bad request error.");
      });
  }

  // getSelectedLocation(selectedLocation){
  //   console.log("selected index",selectedLocation)
  //   let state = this;
  //   axios
  //     .get(apiUrl + `/getatristlineup?location_id=${selectedLocation}`)
  //     .then(function(response) {
  //       state.setState({ list: response.data });
  //     })
  //     .catch(function(error) {
  //       Alert.alert("Alert", "Bad request error.");
  //     });

  // }


  render() {
    var { height, width } = Dimensions.get("window");
    const placeholder = {
      label: 'Pick your location',
      value: null,
      color: '#0000',
    };

    return (
      <View style={{ flex: 1, backgroundColor: "#E8E5E6" }}>
        {/* <View
          style={{ marginTop: 50, flexDirection: "row", alignItems: "center" }}
        >
          <Button
            transparent
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={[{ color: "black", fontSize: 30, marginLeft: 10 }]}
            />
          </Button>
          <Text style={{ marginLeft: 20, fontSize: 20 }}>Artist Lineup</Text>
        </View> */}
        <Header
          hasSegment
          style={[{ backgroundColor: "#E52526", marginTop: 22,height:WP('20')  }]}
        >
          <Left>
            <Button
              transparent
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialIcons"
                name="arrow-back"
                style={[{ color: "white" }]}
              />
            </Button>
          </Left>
          <Body>
            <Text
              style={[{ color: "white", alignSelf: "flex-start", fontSize: 18 }]}
            >
            Artist Lineup
            </Text>
          </Body>
          <Right></Right>
        </Header>
      
    
        <ScrollView
        contentContainerStyle = {{display:'flex',}}
        >
    
          {this.state.list.map((l, i) => (
            <View key={i}
            >
              <View
                style={{
                  backgroundColor: "#D93037",
                  height: 30,
                  justifyContent: "center",
                }}
              >
                <Text style={{ marginLeft: 20, color: "white" }}>{l.day.toUpperCase()}</Text>
              </View>
              <ImageBackground
                style={{
                  flex: 1,
                  height: 200,
                  width: null,
                  justifyContent: "flex-end"
                }}
                source={{ uri: l.image }}
              >
                <View
                  style={{
                    backgroundColor: "rgba(0,0,0,0.5)",
                    minHeight: 50,
                    borderBottomLeftRadius: 10,
                    borderBottomRightRadius: 10
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      marginLeft: 15,
                      fontSize: 14,
                      marginTop: 10
                    }}
                  >
                    {l.location}
                  </Text>
                </View>
              </ImageBackground>
            </View>
          ))}
          
        </ScrollView>
      </View>
    );
  }
}

export default HelpScreen;


{/* <View
style={{
  backgroundColor: "white",
  color: "black",
  width: 300,
  marginLeft: 30,
  height:WP('10'),
  paddingRight: 10,
  borderColor: "black",
  borderWidth: 0.5,
  marginBottom: 15,
  justifyContent:'center'
}}
> */}
    {/* <Picker
    selectedValue={this.state.language}
    style={{ height:WP('5'), width: 300, color: "#656D68" }}
    onValueChange={(itemValue, itemIndex) => this.getSelectedLocation(itemValue)}>
    <Picker.Item label="Pick your location" value="4" />
    <Picker.Item label="Lahore" value="1" />
    <Picker.Item label="Islamabad" value="2" />
    <Picker.Item label="Karachi" value="3" />

  </Picker> */}
    {/* <RNPickerSelect
    
    
  onValueChange={(itemValue, itemIndex) => this.getSelectedLocation(itemValue)}
  placeholder = {placeholder}
  placeholderTextColor = "black"
  items={[
      { label: 'Lahore', value: '1' },
      { label: 'Islamabad', value: '2' },
      { label: 'Karachi', value: '3' },
  ]}/> */}
  {/* </View> */}
  

{/* <View
style={{
  backgroundColor: "white",
  color: "black",
  width: 300,
  marginLeft: 30,
  paddingRight: 10,
  borderColor: "black",
  borderWidth: 0.5,
  marginBottom: 15
}}
>
<Picker
  selectedValue={this.state.language}
  style={{ height: 40, width: 300, color: "#656D68" }}
  onValueChange={(itemValue, itemIndex) => this.getSelectedLocation(itemValue)}>
  <Picker.Item label="Pick your location" value="4" />
  <Picker.Item label="Lahore" value="1" />
  <Picker.Item label="Islamabad" value="2" />
  <Picker.Item label="Karachi" value="3" />

</Picker>
</View> */}
