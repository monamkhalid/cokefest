import React from "react";
import { Segment, Button, Form } from "native-base";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  KeyboardAvoidingView,
  ScrollView,
  Alert,BackHandler,AsyncStorage  
} from "react-native";
import { Header } from "react-navigation";
import { apiUrl } from "../config/api";
import { StackActions, NavigationActions } from 'react-navigation';

import InputText from "../components/InputText";
import background from '../assets/images/artWork.jpeg';
import axios from "axios";

class SignUpScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: ""
    };
  }
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
}

handleBackButton = () => {
    //add your code
    BackHandler.exitApp()

    return true;
};

  verificationCode = () => {
    let { code } = this.state;
    const { navigate } = this.props.navigation;

    if (code.length < 2) {
      Alert.alert("Alert", "Enter a valid code.");
    } else {
      axios
        .get(apiUrl + "/verify_code", {
          params: {
            code: code
          }
        })
        .then((response)=> {
          if (response.data.result == "false") {
            Alert.alert("Alert", "Wrong Code, try again.");
          } else {
            // navigate("SignInScreen", { user_id: response.data.user_id });
            var token = {
              userid:response.data.user_id,
              name:response.data.firstname,
              phoneno: response.data.phone_number,
              lastname:response.data.lastname,
              useremail:response.data.user_email,
              profilepic:null
            }
            this.saveKey(JSON.stringify(token))
            console.log("code response",response)
            const resetAction = StackActions.reset({
              index: 0,
              actions: [NavigationActions.navigate({ routeName: 'WelcomeScreen2' })],
            });
            this.props.navigation.dispatch(resetAction);


          }
        })
        .catch((error)=> {
          console.log("app dev",error)
          Alert.alert("Alert", "Bad request error.");
        });
    }
  };
  async saveKey(value) {
    try {
      await AsyncStorage.setItem('@MySuperStore:key', value);
    } catch (error) {
      console.log("Error saving data" + error);
    }
  }
  render() {
    var { height, width } = Dimensions.get("window");

    return (
      <ImageBackground style={{ flex: 1 }} source={background}>
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          keyboardVerticalOffset={Header.HEIGHT + 90}
          behavior="padding"
        >
          <ScrollView>
            <View style={{ height: height * 0.3 }} />
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  color: "grey",
                  fontSize: 20,
                  marginLeft: 30,
                  fontWeight: "bold"
                }}
              >
                Enter Your Code
              </Text>
              <Form style={{ marginTop: 30 }}>
                <InputText
                  icon="mail-outline"
                  placeholder="******"
                  onChangeText={text => this.setState({ code: text })}
                  value={this.state.email}
                  keyboardType={'phone-pad'}
                  />
              </Form>

              <View style={{ marginTop: 60 }}>
                <TouchableOpacity onPress={this.verificationCode}>
                  <View
                    style={[
                      {
                        backgroundColor: "#df1d25",
                        width: width * 0.7,
                        alignItems: "center",
                        width: 300,
                        paddingVertical: 8,
                        marginBottom: 5,
                        alignSelf: "center"
                      }
                    ]}
                  >
                    <Text
                      style={[
                        {
                          fontSize: 18,
                          textAlign: "center",
                          color: "white",
                          fontWeight: "bold"
                        }
                      ]}
                    >
                      Submit
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

export default SignUpScreen;
