import React from 'react';
import RootStack from './RootStack';
import { View } from 'react-native';

export default class App extends React.Component {
  render() {
    
    return (
      <View style={{ flex: 1 }}>
        <RootStack />
      </View>
    );
  }
}
