import React, { Component } from 'react';
import { View, AsyncStorage, StyleSheet } from 'react-native';


export default class Dummy extends Component {
    constructor(props) {
        super(props);
        this.getKey();
    }

    async getKey() {
        try {
          const value = await AsyncStorage.getItem('@MySuperStore:key');
          if(value){
            // this.props.navigation.navigate("DashboardScreen")
            this.props.navigation.navigate('SignInScreen');
    
    
    
    
          }
          else {
            this.props.navigation.navigate('SignInScreen');
    
         
          }
    
        } catch (error) {
          console.log("Error retrieving data" + error);
        }
      }
    render() {
        return (
            <View style={styles.container}>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

