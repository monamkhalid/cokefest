import React from 'react';
import { View, Text, TextInput } from 'react-native';

const BookStall = props => {
  const {
    placeholder,
    value,
    onChangeText,
    style,
    secureTextEntry,
    autoCapitalize
  } = props;
  return (
    <View>
      <TextInput
        placeholderTextColor="#656D68"
        underlineColorAndroid="transparent"
        returnKeyType={'next'}
        style={[style,{ backgroundColor: 'white',
        color: 'black',
        width: 300,
        paddingVertical: 4,
        paddingLeft: 10,
        paddingBottom: 4,
        paddingRight: 10,borderColor:"black",borderWidth:0.5,}]}
        autoCapitalize={autoCapitalize}
        placeholder={placeholder}
        value={value}
        secureTextEntry={secureTextEntry}
        onChangeText={onChangeText}
        keyboardType = {props.keyboardType}
      />
    </View>
  );
};

export default BookStall;