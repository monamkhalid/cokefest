import React from "react";
import { View, Image, Dimensions, KeyboardAvoidingView,
  ScrollView } from "react-native";
import { Item, Input, Label, Icon } from "native-base";

const InputText = props => {
  const {
    placeholder,
    value,
    onChangeText,
    style,
    secureTextEntry,
    autoCapitalize,
    id
  } = props;
  var { height, width } = Dimensions.get("window");

  return (
    <View style={{ marginLeft: 20, marginRight: 20 }}>
      <Item style={{ borderColor: "grey" }}>
        <Icon
          type="MaterialIcons"
          name={props.icon}
          style={[{ fontSize: 18, top: 15, color: 'grey' }]}
        />
        {/* <Label>{props.placeholder}</Label> */}
        {/* <Image
          resizeMode={"contain"}
          style={props.style}
          source={props.icons}
        /> */}

        <Input
          style={[{ top: 15, fontSize: 14, color: "grey" }]}
          placeholder={props.placeholder}
          placeholderTextColor="grey"
          underlineColorAndroid="transparent"
          secureTextEntry={secureTextEntry}
          onChangeText={onChangeText}
          value={value}
          autoCorrect={false}
          autoCapitalize="none"
          {...props}
        />
      </Item>
    </View>
  );
};

export default InputText;
