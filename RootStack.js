import React from "react";
import {
  View,
  ActivityIndicator,
  AsyncStorage,
  Image,
  Text,
  Dimensions,
  TouchableOpacity,ScrollView,TouchableWithoutFeedback
} from "react-native";
import {
  createStackNavigator,
  createSwitchNavigator,
  createDrawerNavigator,
  DrawerItems
} from "react-navigation";
import axios from "axios";
import Naviagtionlist from './screens/Naviagtionlist.js';
import SignInScreen from "./screens/SignInScreen";
import SignUpScreen from "./screens/SignUpScreen";
import ForgotScreen from "./screens/ForgotScreen";
import VerificationScreen from "./screens/VerificationScreen";
import RetypeScreen from "./screens/RetypeScreen";
import DashboardScreen from "./screens/DashboardScreen";
import BuyTicket from "./screens/BuyTicket";
import { Font } from "expo";
import FindFriends from "./screens/FindFriends";
import Socialfeed from "./screens/Socialfeed";
import WelcomeScreen from "./screens/WelcomeScreen";
import FoodStall from "./screens/FoodStall";
import HelpScreen from "./screens/HelpScreen";
import Merchandise from "./screens/Merchandise";
import BookStall from "./screens/BookStall";
import LineupScreen from "./screens/LineUpScreen";
import ScheduleScreen from "./screens/ScheduleScreen";
import RulesScreen from "./screens/RulesScreen";
import WelcomeScreen2 from "./screens/WelcomeScreen2";
import EditProfile from "./screens/EditProfile";
import Navigation from './screens/Navigation';
import { Container, Header, Content, Icon } from "native-base";
var { height, width } = Dimensions.get("window");
import {WP,HP} from './responsive';
import experience from './assets/images/experience.png';
import foodstall from './assets/images/foodstallgood.png'
import Complete from './screens/Complete';
import ImageDetails from './screens/imageDetails';
import FullScreenVideo from './screens/showFullScreenVideo'
const routes = {
  SignInScreen: {
    screen: SignInScreen
  },
  SignUpScreen: {
    screen: SignUpScreen
  },
  ForgotScreen: {
    screen: ForgotScreen
  },
  VerificationScreen: {
    screen: VerificationScreen
  },
  RetypeScreen: {
    screen: RetypeScreen
  },
  DashboardScreen: {
    screen: DashboardScreen
  },
  BuyTicket: {
    screen: BuyTicket
  },
  FindFriends: {
    screen: FindFriends
  },
  Socialfeed: {
    screen: Socialfeed
  },
  WelcomeScreen: {
    screen: WelcomeScreen
  },
  FoodStall: {
    screen: FoodStall
  },
  HelpScreen: {
    screen: HelpScreen
  },
  Merchandise: {
    screen: Merchandise
  },
  BookStall: {
    screen: BookStall
  },
  LineupScreen: {
    screen: LineupScreen
  },
  ScheduleScreen: {
    screen: ScheduleScreen
  },
  RulesScreen: {
    screen: RulesScreen
  },
  WelcomeScreen2: {
    screen: WelcomeScreen2
  },
  EditProfile:{
    screen:EditProfile
  },
  Navigation:{
    screen:Navigation,
  },
  Navigationlist:{
    screen :Naviagtionlist
  },
  Complete:{
    screen:Complete
  },
  ImageDetails:{
    screen:ImageDetails

  },
  FullScreenVideo:{
    screen:FullScreenVideo
  }
};

class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
  }

  async componentDidMount() {
    await Font.loadAsync({
      Ionicons: require("./assets/fonts/ionicons.ttf"),
      "Material Icons": require("./assets/fonts/MaterialIcons-Regular.ttf"),
      MaterialIcons: require("./assets/fonts/MaterialIcons-Regular.ttf"),
      Roboto_medium: require("./assets/fonts/Roboto-Medium.ttf"),
      Roboto_medium: require("./assets/fonts/Roboto-Medium.ttf")
    });
  }

  render() {
    console.disableYellowBox = true;
    return null;
  }
}

const AppStack = createStackNavigator(routes, {
  headerMode: "none",
  navigationOptions: {
    headerVisible: false
  },
  initialRouteName: "WelcomeScreen"
});

const DrawerContent = props => (
  <ScrollView contentContainerStyle={{flex:1,height:height}}>
    <View
      style={{
        display:'flex',
        backgroundColor: "rgba(255,255,255, 0.8)",
        height: WP('45'),
        justifyContent:"flex-end",

      }}
    >
    <TouchableWithoutFeedback
    onPress = {()=>{ props.navigation.goBack()}}
    >
      <Image
        source={require("./assets/images/logo.png")}
        style={{ width: WP('40'), height: WP('40'), resizeMode: "contain" }}
      />
      </TouchableWithoutFeedback>
    </View>
    <View
      style={{
        height: "84%",
        backgroundColor: "rgba(255,255,255, 0.8)"
      }}
    >
      <View
        style={{ flexDirection: "row", alignItems: "center", marginLeft: 5 }}
      >
        <Image
          source={require("./assets/images/connect.png")}
          style={{
            height: WP('10'),
            width: WP('10'),
            resizeMode :"contain"
          }}
          
        />
                <Text style={{ color: "#000" }}>Connect</Text>

      </View>
      <View
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "gray",
          marginRight: 20,
          marginLeft: 5
        }}
      />
      <View style={{flexDirection: "row",display:'flex',alignItems:'center',justifyContent:"space-around" }}>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("BuyTicket");
          }}
        >
          <Image
            source={require("./assets/images/Buy-your-ticket1.png")}
            style={{
              height: WP('20'),
              width: WP('20'),
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("BookStall");
          }}
        >
          <Image
            source={require("./assets/images/Book-your-stall.png")}
            style={{
              height: WP('20'),
              width: WP('20'),
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity>
        {/* <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("FindFriends");
          }}
        >
          <Image
            source={require("./assets/images/find-my-friend.png")}
            style={{
                  height: WP('20'),
              width: WP('20'),
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity> */}
      </View>
      <View
        style={{ flexDirection: "row", alignItems: "center", marginLeft: 5 }}
      >
        <Image
          source={experience}
          style={{
            height: WP('10'),
            width: WP('10'),
            resizeMode :"contain"
          }}
        />
        <Text style={{ color: "#000" }}>Experience</Text>
      </View>
      <View
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "gray",
          marginRight: 20,
          marginLeft: 5
        }}
      />
      <View style={{flexDirection: "row",display:'flex',alignItems:'center',justifyContent:"space-around" }}>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("ScheduleScreen");
          }}
        >
          <Image
            source={require("./assets/images/Schedule.png")}
            style={{
              height: WP('20'),
              width: WP('20'),
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("Navigation");
          }}
        >
          <Image
            source={require("./assets/images/navigation.png")}
            style={{
              height: WP('20'),
              width: WP('20'),
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("FoodStall");
          }}
        >
          <Image
            source={foodstall}
            style={{
              height: WP('20'),
              width: WP('20'),
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: "row",display:'flex',alignItems:'center',justifyContent:"space-around" }}>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("LineupScreen");
          }}
        >
          <Image
            source={require("./assets/images/Music-Lineup-draw.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            props.navigation.navigate("Merchandise");
          }}
        >
          <Image
            source={require("./assets/images/gallery.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity>
        <View
        style = {{ height: height * 0.1,
          width: width * 0.25,}}
        onPress = {()=>{props.navigation.navigate('Complete')}}
        >
          {/* <Image
            source={require("./assets/images/win.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          /> */}
        </View>
      </View>
      <View
        style={{ flexDirection: "row", alignItems: "center", marginLeft: 5 }}
      >
        <Image
          source={require("./assets/images/Help.png")}
          style={{
            height: WP('10'),
            width: WP('10'),
            resizeMode :"contain"
          }}
        />
        <Text style={{ color: "#000" }}>Help</Text>
      </View>
      <View
        style={{
          borderBottomWidth: 1,
          borderBottomColor: "gray",
          marginRight: 20,
          marginLeft: 5
        }}
      />
      <View style={{flexDirection: "row",display:'flex',alignItems:'center',justifyContent:"space-around" }}>
        <TouchableOpacity 
        style= {{display:'flex',alignItems:'center'}}
        onPress={() => {
            props.navigation.navigate("HelpScreen");
          }}>
          <Image
            source={require("./assets/images/FAQs.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity  onPress={() => {
            props.navigation.navigate("RulesScreen");
          }}>
          <Image
            source={require("./assets/images/rules.png")}
            style={{
              height: height * 0.1,
              width: width * 0.25,
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity>
      </View>
      <View style={{flexDirection: "row",display:'flex',alignItems:'center',justifyContent:"center" }}>
        {/* <TouchableOpacity>
      <Image
        source={require("./assets/images/Logo-1.png")}
        style={{height: height * 0.15,
          width: width * 0.35, resizeMode: "contain" }}
      />
        </TouchableOpacity> */}
        <TouchableOpacity>
       <Image
        source={require("./assets/images/Logo2.png")}
        style={{ height: height * 0.15,
          width: width * 0.35, resizeMode: "contain" }}
      />
      </TouchableOpacity>
     
      </View>
    </View>
  </ScrollView>
);
//  <SvgUri svgXmlData={LogoString} width="150" height="70" fill="white" />
const MainDrawer = createDrawerNavigator(
  {
    DashboardScreen: {
      path: "/",
      screen: AppStack,
      navigationOptions: {
        drawerLockMode: "locked-closed",
        tabBarLabel: "Buttons",
        drawerLabel: <AppStack />
      }
    }
  },
  {
    title: "Welcome",
    initialRouteName: "DashboardScreen",
    drawerPosition: "left",
    drawerOpenRoute: "DrawerOpen",
    drawerCloseRoute: "DrawerClose",
    drawerToggleRoute: "DrawerToggle",
    drawerBackgroundColor: "transparent",
    contentComponent: DrawerContent,
    navigationOptions: {
      headerStyle: {
        backgroundColor: "#f4511e"
      },
      headerTintColor: "green",
      headerTitleStyle: {
        fontWeight: "bold"
      }
    },
    contentOptions: {
      activeTintColor: "black",
      activeBackgroundColor: "white",
      style: {
        backgroundColor: "black"
      }
    }
  }
);

export default createSwitchNavigator(
  {
    AppStack: MainDrawer,
    AuthLoading: AuthLoadingScreen
    // Auth: AuthStack
  },
  {
    initialRouteName: "AppStack"
  }
);
